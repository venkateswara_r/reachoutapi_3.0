'use strict';

describe('Controller Tests', function() {

    describe('ContentMetadata Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockContentMetadata;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockContentMetadata = jasmine.createSpy('MockContentMetadata');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ContentMetadata': MockContentMetadata
            };
            createController = function() {
                $injector.get('$controller')("ContentMetadataDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'reachoutApp:contentMetadataUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
