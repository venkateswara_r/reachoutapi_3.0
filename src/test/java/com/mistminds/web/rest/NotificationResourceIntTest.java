package com.mistminds.web.rest;

import com.mistminds.ReachoutApp;
import com.mistminds.domain.Notification;
import com.mistminds.repository.NotificationRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the NotificationResource REST controller.
 *
 * @see NotificationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReachoutApp.class)
@WebAppConfiguration
@IntegrationTest
public class NotificationResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.format(DEFAULT_CREATED);

    private static final ZonedDateTime DEFAULT_DELETED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DELETED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DELETED_STR = dateTimeFormatter.format(DEFAULT_DELETED);

    private static final ZonedDateTime DEFAULT_LAST_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_UPDATE_STR = dateTimeFormatter.format(DEFAULT_LAST_UPDATE);

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;
    private static final String DEFAULT_CATEGORY = "AAAAA";
    private static final String UPDATED_CATEGORY = "BBBBB";
    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final Boolean DEFAULT_OFFENSIVE = false;
    private static final Boolean UPDATED_OFFENSIVE = true;
    private static final String DEFAULT_IMAGE_URL = "AAAAA";
    private static final String UPDATED_IMAGE_URL = "BBBBB";

    private static final Boolean DEFAULT_ALLOW_CALLS = false;
    private static final Boolean UPDATED_ALLOW_CALLS = true;

    private static final Boolean DEFAULT_SHOW_LOCATION = false;
    private static final Boolean UPDATED_SHOW_LOCATION = true;

    private static final ZonedDateTime DEFAULT_APPROVED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_APPROVED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_APPROVED_STR = dateTimeFormatter.format(DEFAULT_APPROVED);

    private static final Integer DEFAULT_RADIUS = 1;
    private static final Integer UPDATED_RADIUS = 2;

    private static final Integer DEFAULT_FREE_CREDITS_USED = 1;
    private static final Integer UPDATED_FREE_CREDITS_USED = 2;

    private static final Integer DEFAULT_WALLET_CREDITS_USED = 1;
    private static final Integer UPDATED_WALLET_CREDITS_USED = 2;

    private static final ZonedDateTime DEFAULT_VALID_FROM = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_VALID_FROM = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_VALID_FROM_STR = dateTimeFormatter.format(DEFAULT_VALID_FROM);

    private static final ZonedDateTime DEFAULT_VALID_TO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_VALID_TO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_VALID_TO_STR = dateTimeFormatter.format(DEFAULT_VALID_TO);
    private static final String DEFAULT_CONSUMER_ID = "AAAAA";
    private static final String UPDATED_CONSUMER_ID = "BBBBB";

    @Inject
    private NotificationRepository notificationRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restNotificationMockMvc;

    private Notification notification;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NotificationResource notificationResource = new NotificationResource();
        ReflectionTestUtils.setField(notificationResource, "notificationRepository", notificationRepository);
        this.restNotificationMockMvc = MockMvcBuilders.standaloneSetup(notificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        notificationRepository.deleteAll();
        notification = new Notification();
        notification.setCreated(DEFAULT_CREATED);
        notification.setDeleted(DEFAULT_DELETED);
        notification.setLastUpdate(DEFAULT_LAST_UPDATE);
        notification.setVersion(DEFAULT_VERSION);
        notification.setCategory(DEFAULT_CATEGORY);
        notification.setTitle(DEFAULT_TITLE);
        notification.setDescription(DEFAULT_DESCRIPTION);
        notification.setOffensive(DEFAULT_OFFENSIVE);
        notification.setImageUrl(DEFAULT_IMAGE_URL);
        notification.setAllowCalls(DEFAULT_ALLOW_CALLS);
        notification.setShowLocation(DEFAULT_SHOW_LOCATION);
        notification.setApproved(DEFAULT_APPROVED);
        notification.setRadius(DEFAULT_RADIUS);
        notification.setFreeCreditsUsed(DEFAULT_FREE_CREDITS_USED);
        notification.setWalletCreditsUsed(DEFAULT_WALLET_CREDITS_USED);
        notification.setValidFrom(DEFAULT_VALID_FROM);
        notification.setValidTo(DEFAULT_VALID_TO);
        notification.setConsumerId(DEFAULT_CONSUMER_ID);
    }

    @Test
    public void createNotification() throws Exception {
        int databaseSizeBeforeCreate = notificationRepository.findAll().size();

        // Create the Notification

        restNotificationMockMvc.perform(post("/api/notifications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notification)))
                .andExpect(status().isCreated());

        // Validate the Notification in the database
        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).hasSize(databaseSizeBeforeCreate + 1);
        Notification testNotification = notifications.get(notifications.size() - 1);
        assertThat(testNotification.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testNotification.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testNotification.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testNotification.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testNotification.getCategory()).isEqualTo(DEFAULT_CATEGORY);
        assertThat(testNotification.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testNotification.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testNotification.isOffensive()).isEqualTo(DEFAULT_OFFENSIVE);
        assertThat(testNotification.getImageUrl()).isEqualTo(DEFAULT_IMAGE_URL);
        assertThat(testNotification.isAllowCalls()).isEqualTo(DEFAULT_ALLOW_CALLS);
        assertThat(testNotification.isShowLocation()).isEqualTo(DEFAULT_SHOW_LOCATION);
        assertThat(testNotification.getApproved()).isEqualTo(DEFAULT_APPROVED);
        assertThat(testNotification.getRadius()).isEqualTo(DEFAULT_RADIUS);
        assertThat(testNotification.getFreeCreditsUsed()).isEqualTo(DEFAULT_FREE_CREDITS_USED);
        assertThat(testNotification.getWalletCreditsUsed()).isEqualTo(DEFAULT_WALLET_CREDITS_USED);
        assertThat(testNotification.getValidFrom()).isEqualTo(DEFAULT_VALID_FROM);
        assertThat(testNotification.getValidTo()).isEqualTo(DEFAULT_VALID_TO);
        assertThat(testNotification.getConsumerId()).isEqualTo(DEFAULT_CONSUMER_ID);
    }

    @Test
    public void getAllNotifications() throws Exception {
        // Initialize the database
        notificationRepository.save(notification);

        // Get all the notifications
        restNotificationMockMvc.perform(get("/api/notifications?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(notification.getId())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED_STR)))
                .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE_STR)))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
                .andExpect(jsonPath("$.[*].category").value(hasItem(DEFAULT_CATEGORY.toString())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].offensive").value(hasItem(DEFAULT_OFFENSIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL.toString())))
                .andExpect(jsonPath("$.[*].allowCalls").value(hasItem(DEFAULT_ALLOW_CALLS.booleanValue())))
                .andExpect(jsonPath("$.[*].showLocation").value(hasItem(DEFAULT_SHOW_LOCATION.booleanValue())))
                .andExpect(jsonPath("$.[*].approved").value(hasItem(DEFAULT_APPROVED_STR)))
                .andExpect(jsonPath("$.[*].radius").value(hasItem(DEFAULT_RADIUS)))
                .andExpect(jsonPath("$.[*].freeCreditsUsed").value(hasItem(DEFAULT_FREE_CREDITS_USED)))
                .andExpect(jsonPath("$.[*].walletCreditsUsed").value(hasItem(DEFAULT_WALLET_CREDITS_USED)))
                .andExpect(jsonPath("$.[*].validFrom").value(hasItem(DEFAULT_VALID_FROM_STR)))
                .andExpect(jsonPath("$.[*].validTo").value(hasItem(DEFAULT_VALID_TO_STR)))
                .andExpect(jsonPath("$.[*].consumerId").value(hasItem(DEFAULT_CONSUMER_ID.toString())));
    }

    @Test
    public void getNotification() throws Exception {
        // Initialize the database
        notificationRepository.save(notification);

        // Get the notification
        restNotificationMockMvc.perform(get("/api/notifications/{id}", notification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(notification.getId()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED_STR))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE_STR))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.category").value(DEFAULT_CATEGORY.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.offensive").value(DEFAULT_OFFENSIVE.booleanValue()))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGE_URL.toString()))
            .andExpect(jsonPath("$.allowCalls").value(DEFAULT_ALLOW_CALLS.booleanValue()))
            .andExpect(jsonPath("$.showLocation").value(DEFAULT_SHOW_LOCATION.booleanValue()))
            .andExpect(jsonPath("$.approved").value(DEFAULT_APPROVED_STR))
            .andExpect(jsonPath("$.radius").value(DEFAULT_RADIUS))
            .andExpect(jsonPath("$.freeCreditsUsed").value(DEFAULT_FREE_CREDITS_USED))
            .andExpect(jsonPath("$.walletCreditsUsed").value(DEFAULT_WALLET_CREDITS_USED))
            .andExpect(jsonPath("$.validFrom").value(DEFAULT_VALID_FROM_STR))
            .andExpect(jsonPath("$.validTo").value(DEFAULT_VALID_TO_STR))
            .andExpect(jsonPath("$.consumerId").value(DEFAULT_CONSUMER_ID.toString()));
    }

    @Test
    public void getNonExistingNotification() throws Exception {
        // Get the notification
        restNotificationMockMvc.perform(get("/api/notifications/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateNotification() throws Exception {
        // Initialize the database
        notificationRepository.save(notification);
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Update the notification
        Notification updatedNotification = new Notification();
        updatedNotification.setId(notification.getId());
        updatedNotification.setCreated(UPDATED_CREATED);
        updatedNotification.setDeleted(UPDATED_DELETED);
        updatedNotification.setLastUpdate(UPDATED_LAST_UPDATE);
        updatedNotification.setVersion(UPDATED_VERSION);
        updatedNotification.setCategory(UPDATED_CATEGORY);
        updatedNotification.setTitle(UPDATED_TITLE);
        updatedNotification.setDescription(UPDATED_DESCRIPTION);
        updatedNotification.setOffensive(UPDATED_OFFENSIVE);
        updatedNotification.setImageUrl(UPDATED_IMAGE_URL);
        updatedNotification.setAllowCalls(UPDATED_ALLOW_CALLS);
        updatedNotification.setShowLocation(UPDATED_SHOW_LOCATION);
        updatedNotification.setApproved(UPDATED_APPROVED);
        updatedNotification.setRadius(UPDATED_RADIUS);
        updatedNotification.setFreeCreditsUsed(UPDATED_FREE_CREDITS_USED);
        updatedNotification.setWalletCreditsUsed(UPDATED_WALLET_CREDITS_USED);
        updatedNotification.setValidFrom(UPDATED_VALID_FROM);
        updatedNotification.setValidTo(UPDATED_VALID_TO);
        updatedNotification.setConsumerId(UPDATED_CONSUMER_ID);

        restNotificationMockMvc.perform(put("/api/notifications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedNotification)))
                .andExpect(status().isOk());

        // Validate the Notification in the database
        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).hasSize(databaseSizeBeforeUpdate);
        Notification testNotification = notifications.get(notifications.size() - 1);
        assertThat(testNotification.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testNotification.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testNotification.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testNotification.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testNotification.getCategory()).isEqualTo(UPDATED_CATEGORY);
        assertThat(testNotification.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testNotification.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testNotification.isOffensive()).isEqualTo(UPDATED_OFFENSIVE);
        assertThat(testNotification.getImageUrl()).isEqualTo(UPDATED_IMAGE_URL);
        assertThat(testNotification.isAllowCalls()).isEqualTo(UPDATED_ALLOW_CALLS);
        assertThat(testNotification.isShowLocation()).isEqualTo(UPDATED_SHOW_LOCATION);
        assertThat(testNotification.getApproved()).isEqualTo(UPDATED_APPROVED);
        assertThat(testNotification.getRadius()).isEqualTo(UPDATED_RADIUS);
        assertThat(testNotification.getFreeCreditsUsed()).isEqualTo(UPDATED_FREE_CREDITS_USED);
        assertThat(testNotification.getWalletCreditsUsed()).isEqualTo(UPDATED_WALLET_CREDITS_USED);
        assertThat(testNotification.getValidFrom()).isEqualTo(UPDATED_VALID_FROM);
        assertThat(testNotification.getValidTo()).isEqualTo(UPDATED_VALID_TO);
        assertThat(testNotification.getConsumerId()).isEqualTo(UPDATED_CONSUMER_ID);
    }

    @Test
    public void deleteNotification() throws Exception {
        // Initialize the database
        notificationRepository.save(notification);
        int databaseSizeBeforeDelete = notificationRepository.findAll().size();

        // Get the notification
        restNotificationMockMvc.perform(delete("/api/notifications/{id}", notification.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Notification> notifications = notificationRepository.findAll();
        assertThat(notifications).hasSize(databaseSizeBeforeDelete - 1);
    }
}
