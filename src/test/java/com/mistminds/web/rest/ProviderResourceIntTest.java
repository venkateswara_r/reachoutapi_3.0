package com.mistminds.web.rest;

import com.mistminds.ReachoutApp;
import com.mistminds.domain.Provider;
import com.mistminds.repository.ProviderRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.hasItem;

import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ProviderResource REST controller.
 *
 * @see ProviderResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReachoutApp.class)
@WebAppConfiguration
@IntegrationTest
public class ProviderResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.format(DEFAULT_CREATED);

    private static final ZonedDateTime DEFAULT_DELETED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DELETED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DELETED_STR = dateTimeFormatter.format(DEFAULT_DELETED);

    private static final ZonedDateTime DEFAULT_LAST_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_UPDATE_STR = dateTimeFormatter.format(DEFAULT_LAST_UPDATE);

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;
    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_MOBILE = "AAAAA";
    private static final String UPDATED_MOBILE = "BBBBB";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String DEFAULT_ADDRESS = "AAAAA";
    private static final String UPDATED_ADDRESS = "BBBBB";
    private static final String DEFAULT_IMAGE_URL = "AAAAA";
    private static final String UPDATED_IMAGE_URL = "BBBBB";

    private static final ZonedDateTime DEFAULT_PROMO_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_PROMO_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_PROMO_CREATED_DATE_STR = dateTimeFormatter.format(DEFAULT_PROMO_CREATED_DATE);

    private static final Integer DEFAULT_MONTHLY_FREE_CREDITS = 1;
    private static final Integer UPDATED_MONTHLY_FREE_CREDITS = 2;

    private static final Integer DEFAULT_WALLET_CREDITS = 1;
    private static final Integer UPDATED_WALLET_CREDITS = 2;

    private static final Boolean DEFAULT_ELIGIBLE_FOR_PROMO_CREDIT = false;
    private static final Boolean UPDATED_ELIGIBLE_FOR_PROMO_CREDIT = true;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;
    private static final List<Double> DEFAULT_LOCATION = new ArrayList<Double>();
    private static final List<Double> UPDATED_LOCATION = new ArrayList<Double>();
    private static final String DEFAULT_CONSUMER_ID = "AAAAA";
    private static final String UPDATED_CONSUMER_ID = "BBBBB";

    @Inject
    private ProviderRepository providerRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restProviderMockMvc;

    private Provider provider;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProviderResource providerResource = new ProviderResource();
        ReflectionTestUtils.setField(providerResource, "providerRepository", providerRepository);
        this.restProviderMockMvc = MockMvcBuilders.standaloneSetup(providerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        providerRepository.deleteAll();
        provider = new Provider();
        provider.setCreated(DEFAULT_CREATED);
        provider.setDeleted(DEFAULT_DELETED);
        provider.setLastUpdate(DEFAULT_LAST_UPDATE);
        provider.setVersion(DEFAULT_VERSION);
        provider.setName(DEFAULT_NAME);
        provider.setMobile(DEFAULT_MOBILE);
        provider.setEmail(DEFAULT_EMAIL);
        provider.setAddress(DEFAULT_ADDRESS);
        provider.setImageUrl(DEFAULT_IMAGE_URL);
        provider.setPromoCreatedDate(DEFAULT_PROMO_CREATED_DATE);
        provider.setMonthlyFreeCredits(DEFAULT_MONTHLY_FREE_CREDITS);
        provider.setWalletCredits(DEFAULT_WALLET_CREDITS);
        provider.setEligibleForPromoCredit(DEFAULT_ELIGIBLE_FOR_PROMO_CREDIT);
        provider.setActive(DEFAULT_ACTIVE);
        provider.setLocation(DEFAULT_LOCATION);
        provider.setConsumerId(DEFAULT_CONSUMER_ID);
    }

    @Test
    public void createProvider() throws Exception {
        int databaseSizeBeforeCreate = providerRepository.findAll().size();

        // Create the Provider

        restProviderMockMvc.perform(post("/api/providers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(provider)))
                .andExpect(status().isCreated());

        // Validate the Provider in the database
        List<Provider> providers = providerRepository.findAll();
        assertThat(providers).hasSize(databaseSizeBeforeCreate + 1);
        Provider testProvider = providers.get(providers.size() - 1);
        assertThat(testProvider.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testProvider.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testProvider.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testProvider.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testProvider.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProvider.getMobile()).isEqualTo(DEFAULT_MOBILE);
        assertThat(testProvider.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testProvider.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testProvider.getImageUrl()).isEqualTo(DEFAULT_IMAGE_URL);
        assertThat(testProvider.getPromoCreatedDate()).isEqualTo(DEFAULT_PROMO_CREATED_DATE);
        assertThat(testProvider.getMonthlyFreeCredits()).isEqualTo(DEFAULT_MONTHLY_FREE_CREDITS);
        assertThat(testProvider.getWalletCredits()).isEqualTo(DEFAULT_WALLET_CREDITS);
        assertThat(testProvider.isEligibleForPromoCredit()).isEqualTo(DEFAULT_ELIGIBLE_FOR_PROMO_CREDIT);
        assertThat(testProvider.isActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testProvider.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testProvider.getConsumerId()).isEqualTo(DEFAULT_CONSUMER_ID);
    }

    @Test
    public void getAllProviders() throws Exception {
        // Initialize the database
        providerRepository.save(provider);

        // Get all the providers
        restProviderMockMvc.perform(get("/api/providers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(provider.getId())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED_STR)))
                .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE_STR)))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].mobile").value(hasItem(DEFAULT_MOBILE.toString())))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
                .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
                .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL.toString())))
                .andExpect(jsonPath("$.[*].promoCreatedDate").value(hasItem(DEFAULT_PROMO_CREATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].monthlyFreeCredits").value(hasItem(DEFAULT_MONTHLY_FREE_CREDITS)))
                .andExpect(jsonPath("$.[*].walletCredits").value(hasItem(DEFAULT_WALLET_CREDITS)))
                .andExpect(jsonPath("$.[*].eligibleForPromoCredit").value(hasItem(DEFAULT_ELIGIBLE_FOR_PROMO_CREDIT.booleanValue())))
                .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
                .andExpect(jsonPath("$.[*].consumerId").value(hasItem(DEFAULT_CONSUMER_ID.toString())));
    }

    @Test
    public void getProvider() throws Exception {
        // Initialize the database
        providerRepository.save(provider);

        // Get the provider
        restProviderMockMvc.perform(get("/api/providers/{id}", provider.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(provider.getId()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED_STR))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE_STR))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.mobile").value(DEFAULT_MOBILE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGE_URL.toString()))
            .andExpect(jsonPath("$.promoCreatedDate").value(DEFAULT_PROMO_CREATED_DATE_STR))
            .andExpect(jsonPath("$.monthlyFreeCredits").value(DEFAULT_MONTHLY_FREE_CREDITS))
            .andExpect(jsonPath("$.walletCredits").value(DEFAULT_WALLET_CREDITS))
            .andExpect(jsonPath("$.eligibleForPromoCredit").value(DEFAULT_ELIGIBLE_FOR_PROMO_CREDIT.booleanValue()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.consumerId").value(DEFAULT_CONSUMER_ID.toString()));
    }

    @Test
    public void getNonExistingProvider() throws Exception {
        // Get the provider
        restProviderMockMvc.perform(get("/api/providers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateProvider() throws Exception {
        // Initialize the database
        providerRepository.save(provider);
        int databaseSizeBeforeUpdate = providerRepository.findAll().size();

        // Update the provider
        Provider updatedProvider = new Provider();
        updatedProvider.setId(provider.getId());
        updatedProvider.setCreated(UPDATED_CREATED);
        updatedProvider.setDeleted(UPDATED_DELETED);
        updatedProvider.setLastUpdate(UPDATED_LAST_UPDATE);
        updatedProvider.setVersion(UPDATED_VERSION);
        updatedProvider.setName(UPDATED_NAME);
        updatedProvider.setMobile(UPDATED_MOBILE);
        updatedProvider.setEmail(UPDATED_EMAIL);
        updatedProvider.setAddress(UPDATED_ADDRESS);
        updatedProvider.setImageUrl(UPDATED_IMAGE_URL);
        updatedProvider.setPromoCreatedDate(UPDATED_PROMO_CREATED_DATE);
        updatedProvider.setMonthlyFreeCredits(UPDATED_MONTHLY_FREE_CREDITS);
        updatedProvider.setWalletCredits(UPDATED_WALLET_CREDITS);
        updatedProvider.setEligibleForPromoCredit(UPDATED_ELIGIBLE_FOR_PROMO_CREDIT);
        updatedProvider.setActive(UPDATED_ACTIVE);
        updatedProvider.setLocation(UPDATED_LOCATION);
        updatedProvider.setConsumerId(UPDATED_CONSUMER_ID);

        restProviderMockMvc.perform(put("/api/providers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedProvider)))
                .andExpect(status().isOk());

        // Validate the Provider in the database
        List<Provider> providers = providerRepository.findAll();
        assertThat(providers).hasSize(databaseSizeBeforeUpdate);
        Provider testProvider = providers.get(providers.size() - 1);
        assertThat(testProvider.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testProvider.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testProvider.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testProvider.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testProvider.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProvider.getMobile()).isEqualTo(UPDATED_MOBILE);
        assertThat(testProvider.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testProvider.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testProvider.getImageUrl()).isEqualTo(UPDATED_IMAGE_URL);
        assertThat(testProvider.getPromoCreatedDate()).isEqualTo(UPDATED_PROMO_CREATED_DATE);
        assertThat(testProvider.getMonthlyFreeCredits()).isEqualTo(UPDATED_MONTHLY_FREE_CREDITS);
        assertThat(testProvider.getWalletCredits()).isEqualTo(UPDATED_WALLET_CREDITS);
        assertThat(testProvider.isEligibleForPromoCredit()).isEqualTo(UPDATED_ELIGIBLE_FOR_PROMO_CREDIT);
        assertThat(testProvider.isActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testProvider.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testProvider.getConsumerId()).isEqualTo(UPDATED_CONSUMER_ID);
    }

    @Test
    public void deleteProvider() throws Exception {
        // Initialize the database
        providerRepository.save(provider);
        int databaseSizeBeforeDelete = providerRepository.findAll().size();

        // Get the provider
        restProviderMockMvc.perform(delete("/api/providers/{id}", provider.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Provider> providers = providerRepository.findAll();
        assertThat(providers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
