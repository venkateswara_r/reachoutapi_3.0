package com.mistminds.web.rest;

import com.mistminds.ReachoutApp;
import com.mistminds.domain.ConsumerRegions;
import com.mistminds.domain.Region;
import com.mistminds.repository.ConsumerRegionsRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.hasItem;

import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ConsumerRegionsResource REST controller.
 *
 * @see ConsumerRegionsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReachoutApp.class)
@WebAppConfiguration
@IntegrationTest
public class ConsumerRegionsResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.format(DEFAULT_CREATED);

    private static final ZonedDateTime DEFAULT_DELETED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DELETED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DELETED_STR = dateTimeFormatter.format(DEFAULT_DELETED);

    private static final ZonedDateTime DEFAULT_LAST_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_UPDATE_STR = dateTimeFormatter.format(DEFAULT_LAST_UPDATE);

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;
    private static final String DEFAULT_CONSUMER_ID = "AAAAA";
    private static final String UPDATED_CONSUMER_ID = "BBBBB";
    private static final List<Region> DEFAULT_REGION = new ArrayList<Region>();
    private static final List<Region> UPDATED_REGION = new ArrayList<Region>();

    @Inject
    private ConsumerRegionsRepository consumerRegionsRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restConsumerRegionsMockMvc;

    private ConsumerRegions consumerRegions;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ConsumerRegionsResource consumerRegionsResource = new ConsumerRegionsResource();
        ReflectionTestUtils.setField(consumerRegionsResource, "consumerRegionsRepository", consumerRegionsRepository);
        this.restConsumerRegionsMockMvc = MockMvcBuilders.standaloneSetup(consumerRegionsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        consumerRegionsRepository.deleteAll();
        consumerRegions = new ConsumerRegions();
        consumerRegions.setCreated(DEFAULT_CREATED);
        consumerRegions.setDeleted(DEFAULT_DELETED);
        consumerRegions.setLastUpdate(DEFAULT_LAST_UPDATE);
        consumerRegions.setVersion(DEFAULT_VERSION);
        consumerRegions.setConsumerId(DEFAULT_CONSUMER_ID);
        consumerRegions.setRegion(DEFAULT_REGION);
    }

    @Test
    public void createConsumerRegions() throws Exception {
        int databaseSizeBeforeCreate = consumerRegionsRepository.findAll().size();

        // Create the ConsumerRegions

        restConsumerRegionsMockMvc.perform(post("/api/consumer-regions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(consumerRegions)))
                .andExpect(status().isCreated());

        // Validate the ConsumerRegions in the database
        List<ConsumerRegions> consumerRegions = consumerRegionsRepository.findAll();
        assertThat(consumerRegions).hasSize(databaseSizeBeforeCreate + 1);
        ConsumerRegions testConsumerRegions = consumerRegions.get(consumerRegions.size() - 1);
        assertThat(testConsumerRegions.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testConsumerRegions.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testConsumerRegions.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testConsumerRegions.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testConsumerRegions.getConsumerId()).isEqualTo(DEFAULT_CONSUMER_ID);
        assertThat(testConsumerRegions.getRegion()).isEqualTo(DEFAULT_REGION);
    }

    @Test
    public void getAllConsumerRegions() throws Exception {
        // Initialize the database
        consumerRegionsRepository.save(consumerRegions);

        // Get all the consumerRegions
        restConsumerRegionsMockMvc.perform(get("/api/consumer-regions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(consumerRegions.getId())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED_STR)))
                .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE_STR)))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
                .andExpect(jsonPath("$.[*].consumerId").value(hasItem(DEFAULT_CONSUMER_ID.toString())))
                .andExpect(jsonPath("$.[*].region").value(hasItem(DEFAULT_REGION.toString())));
    }

    @Test
    public void getConsumerRegions() throws Exception {
        // Initialize the database
        consumerRegionsRepository.save(consumerRegions);

        // Get the consumerRegions
        restConsumerRegionsMockMvc.perform(get("/api/consumer-regions/{id}", consumerRegions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(consumerRegions.getId()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED_STR))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE_STR))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.consumerId").value(DEFAULT_CONSUMER_ID.toString()))
            .andExpect(jsonPath("$.region").value(DEFAULT_REGION.toString()));
    }

    @Test
    public void getNonExistingConsumerRegions() throws Exception {
        // Get the consumerRegions
        restConsumerRegionsMockMvc.perform(get("/api/consumer-regions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateConsumerRegions() throws Exception {
        // Initialize the database
        consumerRegionsRepository.save(consumerRegions);
        int databaseSizeBeforeUpdate = consumerRegionsRepository.findAll().size();

        // Update the consumerRegions
        ConsumerRegions updatedConsumerRegions = new ConsumerRegions();
        updatedConsumerRegions.setId(consumerRegions.getId());
        updatedConsumerRegions.setCreated(UPDATED_CREATED);
        updatedConsumerRegions.setDeleted(UPDATED_DELETED);
        updatedConsumerRegions.setLastUpdate(UPDATED_LAST_UPDATE);
        updatedConsumerRegions.setVersion(UPDATED_VERSION);
        updatedConsumerRegions.setConsumerId(UPDATED_CONSUMER_ID);
        updatedConsumerRegions.setRegion(UPDATED_REGION);

        restConsumerRegionsMockMvc.perform(put("/api/consumer-regions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedConsumerRegions)))
                .andExpect(status().isOk());

        // Validate the ConsumerRegions in the database
        List<ConsumerRegions> consumerRegions = consumerRegionsRepository.findAll();
        assertThat(consumerRegions).hasSize(databaseSizeBeforeUpdate);
        ConsumerRegions testConsumerRegions = consumerRegions.get(consumerRegions.size() - 1);
        assertThat(testConsumerRegions.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testConsumerRegions.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testConsumerRegions.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testConsumerRegions.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testConsumerRegions.getConsumerId()).isEqualTo(UPDATED_CONSUMER_ID);
        assertThat(testConsumerRegions.getRegion()).isEqualTo(UPDATED_REGION);
    }

    @Test
    public void deleteConsumerRegions() throws Exception {
        // Initialize the database
        consumerRegionsRepository.save(consumerRegions);
        int databaseSizeBeforeDelete = consumerRegionsRepository.findAll().size();

        // Get the consumerRegions
        restConsumerRegionsMockMvc.perform(delete("/api/consumer-regions/{id}", consumerRegions.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ConsumerRegions> consumerRegions = consumerRegionsRepository.findAll();
        assertThat(consumerRegions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
