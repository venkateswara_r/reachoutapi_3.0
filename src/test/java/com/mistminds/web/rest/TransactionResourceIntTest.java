package com.mistminds.web.rest;

import com.mistminds.ReachoutApp;
import com.mistminds.domain.Transaction;
import com.mistminds.repository.TransactionRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TransactionResource REST controller.
 *
 * @see TransactionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReachoutApp.class)
@WebAppConfiguration
@IntegrationTest
public class TransactionResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.format(DEFAULT_CREATED);

    private static final ZonedDateTime DEFAULT_DELETED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DELETED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DELETED_STR = dateTimeFormatter.format(DEFAULT_DELETED);

    private static final ZonedDateTime DEFAULT_LAST_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_UPDATE_STR = dateTimeFormatter.format(DEFAULT_LAST_UPDATE);

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;
    private static final String DEFAULT_ORDER_ID = "AAAAA";
    private static final String UPDATED_ORDER_ID = "BBBBB";
    private static final String DEFAULT_TRACKING_ID = "AAAAA";
    private static final String UPDATED_TRACKING_ID = "BBBBB";
    private static final String DEFAULT_BANK_REFERENCE_NUMBER = "AAAAA";
    private static final String UPDATED_BANK_REFERENCE_NUMBER = "BBBBB";
    private static final String DEFAULT_ORDER_STATUS = "AAAAA";
    private static final String UPDATED_ORDER_STATUS = "BBBBB";
    private static final String DEFAULT_FAILURE_MESSAGE = "AAAAA";
    private static final String UPDATED_FAILURE_MESSAGE = "BBBBB";
    private static final String DEFAULT_PAYMENT_MODE = "AAAAA";
    private static final String UPDATED_PAYMENT_MODE = "BBBBB";
    private static final String DEFAULT_CARD_NAME = "AAAAA";
    private static final String UPDATED_CARD_NAME = "BBBBB";
    private static final String DEFAULT_STATUS_CODE = "AAAAA";
    private static final String UPDATED_STATUS_CODE = "BBBBB";
    private static final String DEFAULT_STATUS_MESSAGE = "AAAAA";
    private static final String UPDATED_STATUS_MESSAGE = "BBBBB";

    private static final Double DEFAULT_AMOUNT = 1d;
    private static final Double UPDATED_AMOUNT = 2d;
    private static final String DEFAULT_PROVIDER_ID = "AAAAA";
    private static final String UPDATED_PROVIDER_ID = "BBBBB";
    private static final String DEFAULT_VAULT = "AAAAA";
    private static final String UPDATED_VAULT = "BBBBB";
    private static final String DEFAULT_OFFER_TYPE = "AAAAA";
    private static final String UPDATED_OFFER_TYPE = "BBBBB";
    private static final String DEFAULT_OFFER_CODE = "AAAAA";
    private static final String UPDATED_OFFER_CODE = "BBBBB";
    private static final String DEFAULT_DISCOUNT_VALUE = "AAAAA";
    private static final String UPDATED_DISCOUNT_VALUE = "BBBBB";

    private static final Double DEFAULT_MERCHANT_AMOUNT = 1d;
    private static final Double UPDATED_MERCHANT_AMOUNT = 2d;
    private static final String DEFAULT_ECI_VALUE = "AAAAA";
    private static final String UPDATED_ECI_VALUE = "BBBBB";

    private static final Boolean DEFAULT_RETRY = false;
    private static final Boolean UPDATED_RETRY = true;

    private static final ZonedDateTime DEFAULT_TRANSACTION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_TRANSACTION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_TRANSACTION_DATE_STR = dateTimeFormatter.format(DEFAULT_TRANSACTION_DATE);

    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTransactionMockMvc;

    private Transaction transaction;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TransactionResource transactionResource = new TransactionResource();
        ReflectionTestUtils.setField(transactionResource, "transactionRepository", transactionRepository);
        this.restTransactionMockMvc = MockMvcBuilders.standaloneSetup(transactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        transactionRepository.deleteAll();
        transaction = new Transaction();
        transaction.setCreated(DEFAULT_CREATED);
        transaction.setDeleted(DEFAULT_DELETED);
        transaction.setLastUpdate(DEFAULT_LAST_UPDATE);
        transaction.setVersion(DEFAULT_VERSION);
        transaction.setOrderId(DEFAULT_ORDER_ID);
        transaction.setTrackingId(DEFAULT_TRACKING_ID);
        transaction.setBankReferenceNumber(DEFAULT_BANK_REFERENCE_NUMBER);
        transaction.setOrderStatus(DEFAULT_ORDER_STATUS);
        transaction.setFailureMessage(DEFAULT_FAILURE_MESSAGE);
        transaction.setPaymentMode(DEFAULT_PAYMENT_MODE);
        transaction.setCardName(DEFAULT_CARD_NAME);
        transaction.setStatusCode(DEFAULT_STATUS_CODE);
        transaction.setStatusMessage(DEFAULT_STATUS_MESSAGE);
        transaction.setAmount(DEFAULT_AMOUNT);
        transaction.setProviderId(DEFAULT_PROVIDER_ID);
        transaction.setVault(DEFAULT_VAULT);
        transaction.setOfferType(DEFAULT_OFFER_TYPE);
        transaction.setOfferCode(DEFAULT_OFFER_CODE);
        transaction.setDiscountValue(DEFAULT_DISCOUNT_VALUE);
        transaction.setMerchantAmount(DEFAULT_MERCHANT_AMOUNT);
        transaction.setEciValue(DEFAULT_ECI_VALUE);
        transaction.setRetry(DEFAULT_RETRY);
        transaction.setTransactionDate(DEFAULT_TRANSACTION_DATE);
    }

    @Test
    public void createTransaction() throws Exception {
        int databaseSizeBeforeCreate = transactionRepository.findAll().size();

        // Create the Transaction

        restTransactionMockMvc.perform(post("/api/transactions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(transaction)))
                .andExpect(status().isCreated());

        // Validate the Transaction in the database
        List<Transaction> transactions = transactionRepository.findAll();
        assertThat(transactions).hasSize(databaseSizeBeforeCreate + 1);
        Transaction testTransaction = transactions.get(transactions.size() - 1);
        assertThat(testTransaction.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testTransaction.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testTransaction.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testTransaction.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testTransaction.getOrderId()).isEqualTo(DEFAULT_ORDER_ID);
        assertThat(testTransaction.getTrackingId()).isEqualTo(DEFAULT_TRACKING_ID);
        assertThat(testTransaction.getBankReferenceNumber()).isEqualTo(DEFAULT_BANK_REFERENCE_NUMBER);
        assertThat(testTransaction.getOrderStatus()).isEqualTo(DEFAULT_ORDER_STATUS);
        assertThat(testTransaction.getFailureMessage()).isEqualTo(DEFAULT_FAILURE_MESSAGE);
        assertThat(testTransaction.getPaymentMode()).isEqualTo(DEFAULT_PAYMENT_MODE);
        assertThat(testTransaction.getCardName()).isEqualTo(DEFAULT_CARD_NAME);
        assertThat(testTransaction.getStatusCode()).isEqualTo(DEFAULT_STATUS_CODE);
        assertThat(testTransaction.getStatusMessage()).isEqualTo(DEFAULT_STATUS_MESSAGE);
        assertThat(testTransaction.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testTransaction.getProviderId()).isEqualTo(DEFAULT_PROVIDER_ID);
        assertThat(testTransaction.getVault()).isEqualTo(DEFAULT_VAULT);
        assertThat(testTransaction.getOfferType()).isEqualTo(DEFAULT_OFFER_TYPE);
        assertThat(testTransaction.getOfferCode()).isEqualTo(DEFAULT_OFFER_CODE);
        assertThat(testTransaction.getDiscountValue()).isEqualTo(DEFAULT_DISCOUNT_VALUE);
        assertThat(testTransaction.getMerchantAmount()).isEqualTo(DEFAULT_MERCHANT_AMOUNT);
        assertThat(testTransaction.getEciValue()).isEqualTo(DEFAULT_ECI_VALUE);
        assertThat(testTransaction.isRetry()).isEqualTo(DEFAULT_RETRY);
        assertThat(testTransaction.getTransactionDate()).isEqualTo(DEFAULT_TRANSACTION_DATE);
    }

    @Test
    public void getAllTransactions() throws Exception {
        // Initialize the database
        transactionRepository.save(transaction);

        // Get all the transactions
        restTransactionMockMvc.perform(get("/api/transactions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(transaction.getId())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED_STR)))
                .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE_STR)))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
                .andExpect(jsonPath("$.[*].orderId").value(hasItem(DEFAULT_ORDER_ID.toString())))
                .andExpect(jsonPath("$.[*].trackingId").value(hasItem(DEFAULT_TRACKING_ID.toString())))
                .andExpect(jsonPath("$.[*].bankReferenceNumber").value(hasItem(DEFAULT_BANK_REFERENCE_NUMBER.toString())))
                .andExpect(jsonPath("$.[*].orderStatus").value(hasItem(DEFAULT_ORDER_STATUS.toString())))
                .andExpect(jsonPath("$.[*].failureMessage").value(hasItem(DEFAULT_FAILURE_MESSAGE.toString())))
                .andExpect(jsonPath("$.[*].paymentMode").value(hasItem(DEFAULT_PAYMENT_MODE.toString())))
                .andExpect(jsonPath("$.[*].cardName").value(hasItem(DEFAULT_CARD_NAME.toString())))
                .andExpect(jsonPath("$.[*].statusCode").value(hasItem(DEFAULT_STATUS_CODE.toString())))
                .andExpect(jsonPath("$.[*].statusMessage").value(hasItem(DEFAULT_STATUS_MESSAGE.toString())))
                .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
                .andExpect(jsonPath("$.[*].providerId").value(hasItem(DEFAULT_PROVIDER_ID.toString())))
                .andExpect(jsonPath("$.[*].vault").value(hasItem(DEFAULT_VAULT.toString())))
                .andExpect(jsonPath("$.[*].offerType").value(hasItem(DEFAULT_OFFER_TYPE.toString())))
                .andExpect(jsonPath("$.[*].offerCode").value(hasItem(DEFAULT_OFFER_CODE.toString())))
                .andExpect(jsonPath("$.[*].discountValue").value(hasItem(DEFAULT_DISCOUNT_VALUE.toString())))
                .andExpect(jsonPath("$.[*].merchantAmount").value(hasItem(DEFAULT_MERCHANT_AMOUNT)))
                .andExpect(jsonPath("$.[*].eciValue").value(hasItem(DEFAULT_ECI_VALUE.toString())))
                .andExpect(jsonPath("$.[*].retry").value(hasItem(DEFAULT_RETRY.booleanValue())))
                .andExpect(jsonPath("$.[*].transactionDate").value(hasItem(DEFAULT_TRANSACTION_DATE_STR)));
    }

    @Test
    public void getTransaction() throws Exception {
        // Initialize the database
        transactionRepository.save(transaction);

        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", transaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(transaction.getId()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED_STR))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE_STR))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.orderId").value(DEFAULT_ORDER_ID.toString()))
            .andExpect(jsonPath("$.trackingId").value(DEFAULT_TRACKING_ID.toString()))
            .andExpect(jsonPath("$.bankReferenceNumber").value(DEFAULT_BANK_REFERENCE_NUMBER.toString()))
            .andExpect(jsonPath("$.orderStatus").value(DEFAULT_ORDER_STATUS.toString()))
            .andExpect(jsonPath("$.failureMessage").value(DEFAULT_FAILURE_MESSAGE.toString()))
            .andExpect(jsonPath("$.paymentMode").value(DEFAULT_PAYMENT_MODE.toString()))
            .andExpect(jsonPath("$.cardName").value(DEFAULT_CARD_NAME.toString()))
            .andExpect(jsonPath("$.statusCode").value(DEFAULT_STATUS_CODE.toString()))
            .andExpect(jsonPath("$.statusMessage").value(DEFAULT_STATUS_MESSAGE.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT))
            .andExpect(jsonPath("$.providerId").value(DEFAULT_PROVIDER_ID.toString()))
            .andExpect(jsonPath("$.vault").value(DEFAULT_VAULT.toString()))
            .andExpect(jsonPath("$.offerType").value(DEFAULT_OFFER_TYPE.toString()))
            .andExpect(jsonPath("$.offerCode").value(DEFAULT_OFFER_CODE.toString()))
            .andExpect(jsonPath("$.discountValue").value(DEFAULT_DISCOUNT_VALUE.toString()))
            .andExpect(jsonPath("$.merchantAmount").value(DEFAULT_MERCHANT_AMOUNT))
            .andExpect(jsonPath("$.eciValue").value(DEFAULT_ECI_VALUE.toString()))
            .andExpect(jsonPath("$.retry").value(DEFAULT_RETRY.booleanValue()))
            .andExpect(jsonPath("$.transactionDate").value(DEFAULT_TRANSACTION_DATE_STR));
    }

    @Test
    public void getNonExistingTransaction() throws Exception {
        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateTransaction() throws Exception {
        // Initialize the database
        transactionRepository.save(transaction);
        int databaseSizeBeforeUpdate = transactionRepository.findAll().size();

        // Update the transaction
        Transaction updatedTransaction = new Transaction();
        updatedTransaction.setId(transaction.getId());
        updatedTransaction.setCreated(UPDATED_CREATED);
        updatedTransaction.setDeleted(UPDATED_DELETED);
        updatedTransaction.setLastUpdate(UPDATED_LAST_UPDATE);
        updatedTransaction.setVersion(UPDATED_VERSION);
        updatedTransaction.setOrderId(UPDATED_ORDER_ID);
        updatedTransaction.setTrackingId(UPDATED_TRACKING_ID);
        updatedTransaction.setBankReferenceNumber(UPDATED_BANK_REFERENCE_NUMBER);
        updatedTransaction.setOrderStatus(UPDATED_ORDER_STATUS);
        updatedTransaction.setFailureMessage(UPDATED_FAILURE_MESSAGE);
        updatedTransaction.setPaymentMode(UPDATED_PAYMENT_MODE);
        updatedTransaction.setCardName(UPDATED_CARD_NAME);
        updatedTransaction.setStatusCode(UPDATED_STATUS_CODE);
        updatedTransaction.setStatusMessage(UPDATED_STATUS_MESSAGE);
        updatedTransaction.setAmount(UPDATED_AMOUNT);
        updatedTransaction.setProviderId(UPDATED_PROVIDER_ID);
        updatedTransaction.setVault(UPDATED_VAULT);
        updatedTransaction.setOfferType(UPDATED_OFFER_TYPE);
        updatedTransaction.setOfferCode(UPDATED_OFFER_CODE);
        updatedTransaction.setDiscountValue(UPDATED_DISCOUNT_VALUE);
        updatedTransaction.setMerchantAmount(UPDATED_MERCHANT_AMOUNT);
        updatedTransaction.setEciValue(UPDATED_ECI_VALUE);
        updatedTransaction.setRetry(UPDATED_RETRY);
        updatedTransaction.setTransactionDate(UPDATED_TRANSACTION_DATE);

        restTransactionMockMvc.perform(put("/api/transactions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedTransaction)))
                .andExpect(status().isOk());

        // Validate the Transaction in the database
        List<Transaction> transactions = transactionRepository.findAll();
        assertThat(transactions).hasSize(databaseSizeBeforeUpdate);
        Transaction testTransaction = transactions.get(transactions.size() - 1);
        assertThat(testTransaction.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testTransaction.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testTransaction.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testTransaction.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testTransaction.getOrderId()).isEqualTo(UPDATED_ORDER_ID);
        assertThat(testTransaction.getTrackingId()).isEqualTo(UPDATED_TRACKING_ID);
        assertThat(testTransaction.getBankReferenceNumber()).isEqualTo(UPDATED_BANK_REFERENCE_NUMBER);
        assertThat(testTransaction.getOrderStatus()).isEqualTo(UPDATED_ORDER_STATUS);
        assertThat(testTransaction.getFailureMessage()).isEqualTo(UPDATED_FAILURE_MESSAGE);
        assertThat(testTransaction.getPaymentMode()).isEqualTo(UPDATED_PAYMENT_MODE);
        assertThat(testTransaction.getCardName()).isEqualTo(UPDATED_CARD_NAME);
        assertThat(testTransaction.getStatusCode()).isEqualTo(UPDATED_STATUS_CODE);
        assertThat(testTransaction.getStatusMessage()).isEqualTo(UPDATED_STATUS_MESSAGE);
        assertThat(testTransaction.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testTransaction.getProviderId()).isEqualTo(UPDATED_PROVIDER_ID);
        assertThat(testTransaction.getVault()).isEqualTo(UPDATED_VAULT);
        assertThat(testTransaction.getOfferType()).isEqualTo(UPDATED_OFFER_TYPE);
        assertThat(testTransaction.getOfferCode()).isEqualTo(UPDATED_OFFER_CODE);
        assertThat(testTransaction.getDiscountValue()).isEqualTo(UPDATED_DISCOUNT_VALUE);
        assertThat(testTransaction.getMerchantAmount()).isEqualTo(UPDATED_MERCHANT_AMOUNT);
        assertThat(testTransaction.getEciValue()).isEqualTo(UPDATED_ECI_VALUE);
        assertThat(testTransaction.isRetry()).isEqualTo(UPDATED_RETRY);
        assertThat(testTransaction.getTransactionDate()).isEqualTo(UPDATED_TRANSACTION_DATE);
    }

    @Test
    public void deleteTransaction() throws Exception {
        // Initialize the database
        transactionRepository.save(transaction);
        int databaseSizeBeforeDelete = transactionRepository.findAll().size();

        // Get the transaction
        restTransactionMockMvc.perform(delete("/api/transactions/{id}", transaction.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Transaction> transactions = transactionRepository.findAll();
        assertThat(transactions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
