package com.mistminds.web.rest;

import com.mistminds.ReachoutApp;
import com.mistminds.domain.ApplicationSettings;
import com.mistminds.repository.ApplicationSettingsRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ApplicationSettingsResource REST controller.
 *
 * @see ApplicationSettingsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReachoutApp.class)
@WebAppConfiguration
@IntegrationTest
public class ApplicationSettingsResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.format(DEFAULT_CREATED);

    private static final ZonedDateTime DEFAULT_DELETED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DELETED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DELETED_STR = dateTimeFormatter.format(DEFAULT_DELETED);

    private static final ZonedDateTime DEFAULT_LAST_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_UPDATE_STR = dateTimeFormatter.format(DEFAULT_LAST_UPDATE);

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;
    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_VALUE = "AAAAA";
    private static final String UPDATED_VALUE = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private ApplicationSettingsRepository applicationSettingsRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restApplicationSettingsMockMvc;

    private ApplicationSettings applicationSettings;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ApplicationSettingsResource applicationSettingsResource = new ApplicationSettingsResource();
        ReflectionTestUtils.setField(applicationSettingsResource, "applicationSettingsRepository", applicationSettingsRepository);
        this.restApplicationSettingsMockMvc = MockMvcBuilders.standaloneSetup(applicationSettingsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        applicationSettingsRepository.deleteAll();
        applicationSettings = new ApplicationSettings();
        applicationSettings.setCreated(DEFAULT_CREATED);
        applicationSettings.setDeleted(DEFAULT_DELETED);
        applicationSettings.setLastUpdate(DEFAULT_LAST_UPDATE);
        applicationSettings.setVersion(DEFAULT_VERSION);
        applicationSettings.setName(DEFAULT_NAME);
        applicationSettings.setValue(DEFAULT_VALUE);
        applicationSettings.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    public void createApplicationSettings() throws Exception {
        int databaseSizeBeforeCreate = applicationSettingsRepository.findAll().size();

        // Create the ApplicationSettings

        restApplicationSettingsMockMvc.perform(post("/api/application-settings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(applicationSettings)))
                .andExpect(status().isCreated());

        // Validate the ApplicationSettings in the database
        List<ApplicationSettings> applicationSettings = applicationSettingsRepository.findAll();
        assertThat(applicationSettings).hasSize(databaseSizeBeforeCreate + 1);
        ApplicationSettings testApplicationSettings = applicationSettings.get(applicationSettings.size() - 1);
        assertThat(testApplicationSettings.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testApplicationSettings.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testApplicationSettings.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testApplicationSettings.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testApplicationSettings.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testApplicationSettings.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testApplicationSettings.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    public void getAllApplicationSettings() throws Exception {
        // Initialize the database
        applicationSettingsRepository.save(applicationSettings);

        // Get all the applicationSettings
        restApplicationSettingsMockMvc.perform(get("/api/application-settings?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(applicationSettings.getId())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED_STR)))
                .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE_STR)))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    public void getApplicationSettings() throws Exception {
        // Initialize the database
        applicationSettingsRepository.save(applicationSettings);

        // Get the applicationSettings
        restApplicationSettingsMockMvc.perform(get("/api/application-settings/{id}", applicationSettings.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(applicationSettings.getId()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED_STR))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE_STR))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    public void getNonExistingApplicationSettings() throws Exception {
        // Get the applicationSettings
        restApplicationSettingsMockMvc.perform(get("/api/application-settings/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateApplicationSettings() throws Exception {
        // Initialize the database
        applicationSettingsRepository.save(applicationSettings);
        int databaseSizeBeforeUpdate = applicationSettingsRepository.findAll().size();

        // Update the applicationSettings
        ApplicationSettings updatedApplicationSettings = new ApplicationSettings();
        updatedApplicationSettings.setId(applicationSettings.getId());
        updatedApplicationSettings.setCreated(UPDATED_CREATED);
        updatedApplicationSettings.setDeleted(UPDATED_DELETED);
        updatedApplicationSettings.setLastUpdate(UPDATED_LAST_UPDATE);
        updatedApplicationSettings.setVersion(UPDATED_VERSION);
        updatedApplicationSettings.setName(UPDATED_NAME);
        updatedApplicationSettings.setValue(UPDATED_VALUE);
        updatedApplicationSettings.setDescription(UPDATED_DESCRIPTION);

        restApplicationSettingsMockMvc.perform(put("/api/application-settings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedApplicationSettings)))
                .andExpect(status().isOk());

        // Validate the ApplicationSettings in the database
        List<ApplicationSettings> applicationSettings = applicationSettingsRepository.findAll();
        assertThat(applicationSettings).hasSize(databaseSizeBeforeUpdate);
        ApplicationSettings testApplicationSettings = applicationSettings.get(applicationSettings.size() - 1);
        assertThat(testApplicationSettings.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testApplicationSettings.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testApplicationSettings.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testApplicationSettings.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testApplicationSettings.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testApplicationSettings.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testApplicationSettings.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    public void deleteApplicationSettings() throws Exception {
        // Initialize the database
        applicationSettingsRepository.save(applicationSettings);
        int databaseSizeBeforeDelete = applicationSettingsRepository.findAll().size();

        // Get the applicationSettings
        restApplicationSettingsMockMvc.perform(delete("/api/application-settings/{id}", applicationSettings.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ApplicationSettings> applicationSettings = applicationSettingsRepository.findAll();
        assertThat(applicationSettings).hasSize(databaseSizeBeforeDelete - 1);
    }
}
