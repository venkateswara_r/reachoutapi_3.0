package com.mistminds.web.rest;

import com.mistminds.ReachoutApp;
import com.mistminds.domain.NotificationAcknowledgement;
import com.mistminds.repository.NotificationAcknowledgementRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the NotificationAcknowledgementResource REST controller.
 *
 * @see NotificationAcknowledgementResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReachoutApp.class)
@WebAppConfiguration
@IntegrationTest
public class NotificationAcknowledgementResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.format(DEFAULT_CREATED);

    private static final ZonedDateTime DEFAULT_DELETED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DELETED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DELETED_STR = dateTimeFormatter.format(DEFAULT_DELETED);

    private static final ZonedDateTime DEFAULT_LAST_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_UPDATE_STR = dateTimeFormatter.format(DEFAULT_LAST_UPDATE);

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;

    private static final ZonedDateTime DEFAULT_READ = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_READ = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_READ_STR = dateTimeFormatter.format(DEFAULT_READ);

    private static final ZonedDateTime DEFAULT_DELIVERED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DELIVERED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DELIVERED_STR = dateTimeFormatter.format(DEFAULT_DELIVERED);

    private static final ZonedDateTime DEFAULT_SENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_SENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_SENT_STR = dateTimeFormatter.format(DEFAULT_SENT);

    private static final ZonedDateTime DEFAULT_VISITED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_VISITED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_VISITED_STR = dateTimeFormatter.format(DEFAULT_VISITED);
    private static final String DEFAULT_CONSUMER_ID = "AAAAA";
    private static final String UPDATED_CONSUMER_ID = "BBBBB";
    private static final String DEFAULT_NOTIFICATION_ID = "AAAAA";
    private static final String UPDATED_NOTIFICATION_ID = "BBBBB";

    @Inject
    private NotificationAcknowledgementRepository notificationAcknowledgementRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restNotificationAcknowledgementMockMvc;

    private NotificationAcknowledgement notificationAcknowledgement;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NotificationAcknowledgementResource notificationAcknowledgementResource = new NotificationAcknowledgementResource();
        ReflectionTestUtils.setField(notificationAcknowledgementResource, "notificationAcknowledgementRepository", notificationAcknowledgementRepository);
        this.restNotificationAcknowledgementMockMvc = MockMvcBuilders.standaloneSetup(notificationAcknowledgementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        notificationAcknowledgementRepository.deleteAll();
        notificationAcknowledgement = new NotificationAcknowledgement();
        notificationAcknowledgement.setCreated(DEFAULT_CREATED);
        notificationAcknowledgement.setDeleted(DEFAULT_DELETED);
        notificationAcknowledgement.setLastUpdate(DEFAULT_LAST_UPDATE);
        notificationAcknowledgement.setVersion(DEFAULT_VERSION);
        notificationAcknowledgement.setRead(DEFAULT_READ);
        notificationAcknowledgement.setDelivered(DEFAULT_DELIVERED);
        notificationAcknowledgement.setSent(DEFAULT_SENT);
        notificationAcknowledgement.setVisited(DEFAULT_VISITED);
        notificationAcknowledgement.setConsumerId(DEFAULT_CONSUMER_ID);
        notificationAcknowledgement.setNotificationId(DEFAULT_NOTIFICATION_ID);
    }

    @Test
    public void createNotificationAcknowledgement() throws Exception {
        int databaseSizeBeforeCreate = notificationAcknowledgementRepository.findAll().size();

        // Create the NotificationAcknowledgement

        restNotificationAcknowledgementMockMvc.perform(post("/api/notification-acknowledgements")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notificationAcknowledgement)))
                .andExpect(status().isCreated());

        // Validate the NotificationAcknowledgement in the database
        List<NotificationAcknowledgement> notificationAcknowledgements = notificationAcknowledgementRepository.findAll();
        assertThat(notificationAcknowledgements).hasSize(databaseSizeBeforeCreate + 1);
        NotificationAcknowledgement testNotificationAcknowledgement = notificationAcknowledgements.get(notificationAcknowledgements.size() - 1);
        assertThat(testNotificationAcknowledgement.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testNotificationAcknowledgement.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testNotificationAcknowledgement.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testNotificationAcknowledgement.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testNotificationAcknowledgement.getRead()).isEqualTo(DEFAULT_READ);
        assertThat(testNotificationAcknowledgement.getDelivered()).isEqualTo(DEFAULT_DELIVERED);
        assertThat(testNotificationAcknowledgement.getSent()).isEqualTo(DEFAULT_SENT);
        assertThat(testNotificationAcknowledgement.getVisited()).isEqualTo(DEFAULT_VISITED);
        assertThat(testNotificationAcknowledgement.getConsumerId()).isEqualTo(DEFAULT_CONSUMER_ID);
        assertThat(testNotificationAcknowledgement.getNotificationId()).isEqualTo(DEFAULT_NOTIFICATION_ID);
    }

    @Test
    public void getAllNotificationAcknowledgements() throws Exception {
        // Initialize the database
        notificationAcknowledgementRepository.save(notificationAcknowledgement);

        // Get all the notificationAcknowledgements
        restNotificationAcknowledgementMockMvc.perform(get("/api/notification-acknowledgements?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(notificationAcknowledgement.getId())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED_STR)))
                .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE_STR)))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
                .andExpect(jsonPath("$.[*].read").value(hasItem(DEFAULT_READ_STR)))
                .andExpect(jsonPath("$.[*].delivered").value(hasItem(DEFAULT_DELIVERED_STR)))
                .andExpect(jsonPath("$.[*].sent").value(hasItem(DEFAULT_SENT_STR)))
                .andExpect(jsonPath("$.[*].visited").value(hasItem(DEFAULT_VISITED_STR)))
                .andExpect(jsonPath("$.[*].consumerId").value(hasItem(DEFAULT_CONSUMER_ID.toString())))
                .andExpect(jsonPath("$.[*].notificationId").value(hasItem(DEFAULT_NOTIFICATION_ID.toString())));
    }

    @Test
    public void getNotificationAcknowledgement() throws Exception {
        // Initialize the database
        notificationAcknowledgementRepository.save(notificationAcknowledgement);

        // Get the notificationAcknowledgement
        restNotificationAcknowledgementMockMvc.perform(get("/api/notification-acknowledgements/{id}", notificationAcknowledgement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(notificationAcknowledgement.getId()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED_STR))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE_STR))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.read").value(DEFAULT_READ_STR))
            .andExpect(jsonPath("$.delivered").value(DEFAULT_DELIVERED_STR))
            .andExpect(jsonPath("$.sent").value(DEFAULT_SENT_STR))
            .andExpect(jsonPath("$.visited").value(DEFAULT_VISITED_STR))
            .andExpect(jsonPath("$.consumerId").value(DEFAULT_CONSUMER_ID.toString()))
            .andExpect(jsonPath("$.notificationId").value(DEFAULT_NOTIFICATION_ID.toString()));
    }

    @Test
    public void getNonExistingNotificationAcknowledgement() throws Exception {
        // Get the notificationAcknowledgement
        restNotificationAcknowledgementMockMvc.perform(get("/api/notification-acknowledgements/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateNotificationAcknowledgement() throws Exception {
        // Initialize the database
        notificationAcknowledgementRepository.save(notificationAcknowledgement);
        int databaseSizeBeforeUpdate = notificationAcknowledgementRepository.findAll().size();

        // Update the notificationAcknowledgement
        NotificationAcknowledgement updatedNotificationAcknowledgement = new NotificationAcknowledgement();
        updatedNotificationAcknowledgement.setId(notificationAcknowledgement.getId());
        updatedNotificationAcknowledgement.setCreated(UPDATED_CREATED);
        updatedNotificationAcknowledgement.setDeleted(UPDATED_DELETED);
        updatedNotificationAcknowledgement.setLastUpdate(UPDATED_LAST_UPDATE);
        updatedNotificationAcknowledgement.setVersion(UPDATED_VERSION);
        updatedNotificationAcknowledgement.setRead(UPDATED_READ);
        updatedNotificationAcknowledgement.setDelivered(UPDATED_DELIVERED);
        updatedNotificationAcknowledgement.setSent(UPDATED_SENT);
        updatedNotificationAcknowledgement.setVisited(UPDATED_VISITED);
        updatedNotificationAcknowledgement.setConsumerId(UPDATED_CONSUMER_ID);
        updatedNotificationAcknowledgement.setNotificationId(UPDATED_NOTIFICATION_ID);

        restNotificationAcknowledgementMockMvc.perform(put("/api/notification-acknowledgements")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedNotificationAcknowledgement)))
                .andExpect(status().isOk());

        // Validate the NotificationAcknowledgement in the database
        List<NotificationAcknowledgement> notificationAcknowledgements = notificationAcknowledgementRepository.findAll();
        assertThat(notificationAcknowledgements).hasSize(databaseSizeBeforeUpdate);
        NotificationAcknowledgement testNotificationAcknowledgement = notificationAcknowledgements.get(notificationAcknowledgements.size() - 1);
        assertThat(testNotificationAcknowledgement.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testNotificationAcknowledgement.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testNotificationAcknowledgement.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testNotificationAcknowledgement.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testNotificationAcknowledgement.getRead()).isEqualTo(UPDATED_READ);
        assertThat(testNotificationAcknowledgement.getDelivered()).isEqualTo(UPDATED_DELIVERED);
        assertThat(testNotificationAcknowledgement.getSent()).isEqualTo(UPDATED_SENT);
        assertThat(testNotificationAcknowledgement.getVisited()).isEqualTo(UPDATED_VISITED);
        assertThat(testNotificationAcknowledgement.getConsumerId()).isEqualTo(UPDATED_CONSUMER_ID);
        assertThat(testNotificationAcknowledgement.getNotificationId()).isEqualTo(UPDATED_NOTIFICATION_ID);
    }

    @Test
    public void deleteNotificationAcknowledgement() throws Exception {
        // Initialize the database
        notificationAcknowledgementRepository.save(notificationAcknowledgement);
        int databaseSizeBeforeDelete = notificationAcknowledgementRepository.findAll().size();

        // Get the notificationAcknowledgement
        restNotificationAcknowledgementMockMvc.perform(delete("/api/notification-acknowledgements/{id}", notificationAcknowledgement.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<NotificationAcknowledgement> notificationAcknowledgements = notificationAcknowledgementRepository.findAll();
        assertThat(notificationAcknowledgements).hasSize(databaseSizeBeforeDelete - 1);
    }
}
