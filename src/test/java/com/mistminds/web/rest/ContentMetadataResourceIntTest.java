package com.mistminds.web.rest;

import com.mistminds.ReachoutApp;
import com.mistminds.domain.ContentMetadata;
import com.mistminds.repository.ContentMetadataRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ContentMetadataResource REST controller.
 *
 * @see ContentMetadataResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReachoutApp.class)
@WebAppConfiguration
@IntegrationTest
public class ContentMetadataResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.format(DEFAULT_CREATED);

    private static final ZonedDateTime DEFAULT_DELETED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DELETED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DELETED_STR = dateTimeFormatter.format(DEFAULT_DELETED);

    private static final ZonedDateTime DEFAULT_LAST_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_UPDATE_STR = dateTimeFormatter.format(DEFAULT_LAST_UPDATE);

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;
    private static final String DEFAULT_SIGNATURE = "AAAAA";
    private static final String UPDATED_SIGNATURE = "BBBBB";
    private static final String DEFAULT_FORMAT = "AAAAA";
    private static final String UPDATED_FORMAT = "BBBBB";
    private static final String DEFAULT_TYPE = "AAAAA";
    private static final String UPDATED_TYPE = "BBBBB";
    private static final String DEFAULT_IMAGE_VERSION = "AAAAA";
    private static final String UPDATED_IMAGE_VERSION = "BBBBB";
    private static final String DEFAULT_URL = "AAAAA";
    private static final String UPDATED_URL = "BBBBB";
    private static final String DEFAULT_TAGS = "AAAAA";
    private static final String UPDATED_TAGS = "BBBBB";
    private static final String DEFAULT_BYTES = "AAAAA";
    private static final String UPDATED_BYTES = "BBBBB";
    private static final String DEFAULT_WIDTH = "AAAAA";
    private static final String UPDATED_WIDTH = "BBBBB";
    private static final String DEFAULT_HEIGHT = "AAAAA";
    private static final String UPDATED_HEIGHT = "BBBBB";

    @Inject
    private ContentMetadataRepository contentMetadataRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restContentMetadataMockMvc;

    private ContentMetadata contentMetadata;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ContentMetadataResource contentMetadataResource = new ContentMetadataResource();
        ReflectionTestUtils.setField(contentMetadataResource, "contentMetadataRepository", contentMetadataRepository);
        this.restContentMetadataMockMvc = MockMvcBuilders.standaloneSetup(contentMetadataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        contentMetadataRepository.deleteAll();
        contentMetadata = new ContentMetadata();
        contentMetadata.setCreated(DEFAULT_CREATED);
        contentMetadata.setDeleted(DEFAULT_DELETED);
        contentMetadata.setLastUpdate(DEFAULT_LAST_UPDATE);
        contentMetadata.setVersion(DEFAULT_VERSION);
        contentMetadata.setSignature(DEFAULT_SIGNATURE);
        contentMetadata.setFormat(DEFAULT_FORMAT);
        contentMetadata.setType(DEFAULT_TYPE);
        contentMetadata.setImageVersion(DEFAULT_IMAGE_VERSION);
        contentMetadata.setUrl(DEFAULT_URL);
        contentMetadata.setTags(DEFAULT_TAGS);
        contentMetadata.setBytes(DEFAULT_BYTES);
        contentMetadata.setWidth(DEFAULT_WIDTH);
        contentMetadata.setHeight(DEFAULT_HEIGHT);
    }

    @Test
    public void createContentMetadata() throws Exception {
        int databaseSizeBeforeCreate = contentMetadataRepository.findAll().size();

        // Create the ContentMetadata

        restContentMetadataMockMvc.perform(post("/api/content-metadata")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(contentMetadata)))
                .andExpect(status().isCreated());

        // Validate the ContentMetadata in the database
        List<ContentMetadata> contentMetadata = contentMetadataRepository.findAll();
        assertThat(contentMetadata).hasSize(databaseSizeBeforeCreate + 1);
        ContentMetadata testContentMetadata = contentMetadata.get(contentMetadata.size() - 1);
        assertThat(testContentMetadata.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testContentMetadata.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testContentMetadata.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testContentMetadata.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testContentMetadata.getSignature()).isEqualTo(DEFAULT_SIGNATURE);
        assertThat(testContentMetadata.getFormat()).isEqualTo(DEFAULT_FORMAT);
        assertThat(testContentMetadata.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testContentMetadata.getImageVersion()).isEqualTo(DEFAULT_IMAGE_VERSION);
        assertThat(testContentMetadata.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testContentMetadata.getTags()).isEqualTo(DEFAULT_TAGS);
        assertThat(testContentMetadata.getBytes()).isEqualTo(DEFAULT_BYTES);
        assertThat(testContentMetadata.getWidth()).isEqualTo(DEFAULT_WIDTH);
        assertThat(testContentMetadata.getHeight()).isEqualTo(DEFAULT_HEIGHT);
    }

    @Test
    public void getAllContentMetadata() throws Exception {
        // Initialize the database
        contentMetadataRepository.save(contentMetadata);

        // Get all the contentMetadata
        restContentMetadataMockMvc.perform(get("/api/content-metadata?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(contentMetadata.getId())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED_STR)))
                .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE_STR)))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
                .andExpect(jsonPath("$.[*].signature").value(hasItem(DEFAULT_SIGNATURE.toString())))
                .andExpect(jsonPath("$.[*].format").value(hasItem(DEFAULT_FORMAT.toString())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].imageVersion").value(hasItem(DEFAULT_IMAGE_VERSION.toString())))
                .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
                .andExpect(jsonPath("$.[*].tags").value(hasItem(DEFAULT_TAGS.toString())))
                .andExpect(jsonPath("$.[*].bytes").value(hasItem(DEFAULT_BYTES.toString())))
                .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.toString())))
                .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.toString())));
    }

    @Test
    public void getContentMetadata() throws Exception {
        // Initialize the database
        contentMetadataRepository.save(contentMetadata);

        // Get the contentMetadata
        restContentMetadataMockMvc.perform(get("/api/content-metadata/{id}", contentMetadata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(contentMetadata.getId()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED_STR))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE_STR))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.signature").value(DEFAULT_SIGNATURE.toString()))
            .andExpect(jsonPath("$.format").value(DEFAULT_FORMAT.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.imageVersion").value(DEFAULT_IMAGE_VERSION.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.tags").value(DEFAULT_TAGS.toString()))
            .andExpect(jsonPath("$.bytes").value(DEFAULT_BYTES.toString()))
            .andExpect(jsonPath("$.width").value(DEFAULT_WIDTH.toString()))
            .andExpect(jsonPath("$.height").value(DEFAULT_HEIGHT.toString()));
    }

    @Test
    public void getNonExistingContentMetadata() throws Exception {
        // Get the contentMetadata
        restContentMetadataMockMvc.perform(get("/api/content-metadata/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateContentMetadata() throws Exception {
        // Initialize the database
        contentMetadataRepository.save(contentMetadata);
        int databaseSizeBeforeUpdate = contentMetadataRepository.findAll().size();

        // Update the contentMetadata
        ContentMetadata updatedContentMetadata = new ContentMetadata();
        updatedContentMetadata.setId(contentMetadata.getId());
        updatedContentMetadata.setCreated(UPDATED_CREATED);
        updatedContentMetadata.setDeleted(UPDATED_DELETED);
        updatedContentMetadata.setLastUpdate(UPDATED_LAST_UPDATE);
        updatedContentMetadata.setVersion(UPDATED_VERSION);
        updatedContentMetadata.setSignature(UPDATED_SIGNATURE);
        updatedContentMetadata.setFormat(UPDATED_FORMAT);
        updatedContentMetadata.setType(UPDATED_TYPE);
        updatedContentMetadata.setImageVersion(UPDATED_IMAGE_VERSION);
        updatedContentMetadata.setUrl(UPDATED_URL);
        updatedContentMetadata.setTags(UPDATED_TAGS);
        updatedContentMetadata.setBytes(UPDATED_BYTES);
        updatedContentMetadata.setWidth(UPDATED_WIDTH);
        updatedContentMetadata.setHeight(UPDATED_HEIGHT);

        restContentMetadataMockMvc.perform(put("/api/content-metadata")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedContentMetadata)))
                .andExpect(status().isOk());

        // Validate the ContentMetadata in the database
        List<ContentMetadata> contentMetadata = contentMetadataRepository.findAll();
        assertThat(contentMetadata).hasSize(databaseSizeBeforeUpdate);
        ContentMetadata testContentMetadata = contentMetadata.get(contentMetadata.size() - 1);
        assertThat(testContentMetadata.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testContentMetadata.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testContentMetadata.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testContentMetadata.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testContentMetadata.getSignature()).isEqualTo(UPDATED_SIGNATURE);
        assertThat(testContentMetadata.getFormat()).isEqualTo(UPDATED_FORMAT);
        assertThat(testContentMetadata.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testContentMetadata.getImageVersion()).isEqualTo(UPDATED_IMAGE_VERSION);
        assertThat(testContentMetadata.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testContentMetadata.getTags()).isEqualTo(UPDATED_TAGS);
        assertThat(testContentMetadata.getBytes()).isEqualTo(UPDATED_BYTES);
        assertThat(testContentMetadata.getWidth()).isEqualTo(UPDATED_WIDTH);
        assertThat(testContentMetadata.getHeight()).isEqualTo(UPDATED_HEIGHT);
    }

    @Test
    public void deleteContentMetadata() throws Exception {
        // Initialize the database
        contentMetadataRepository.save(contentMetadata);
        int databaseSizeBeforeDelete = contentMetadataRepository.findAll().size();

        // Get the contentMetadata
        restContentMetadataMockMvc.perform(delete("/api/content-metadata/{id}", contentMetadata.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ContentMetadata> contentMetadata = contentMetadataRepository.findAll();
        assertThat(contentMetadata).hasSize(databaseSizeBeforeDelete - 1);
    }
}
