package com.mistminds.web.rest;

import com.mistminds.ReachoutApp;
import com.mistminds.domain.ConsumerFeedback;
import com.mistminds.repository.ConsumerFeedbackRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ConsumerFeedbackResource REST controller.
 *
 * @see ConsumerFeedbackResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReachoutApp.class)
@WebAppConfiguration
@IntegrationTest
public class ConsumerFeedbackResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.format(DEFAULT_CREATED);

    private static final ZonedDateTime DEFAULT_DELETED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DELETED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DELETED_STR = dateTimeFormatter.format(DEFAULT_DELETED);

    private static final ZonedDateTime DEFAULT_LAST_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_LAST_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_LAST_UPDATE_STR = dateTimeFormatter.format(DEFAULT_LAST_UPDATE);

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;
    private static final String DEFAULT_COMMENT = "AAAAA";
    private static final String UPDATED_COMMENT = "BBBBB";
    private static final String DEFAULT_LIKE_DISLIKE = "AAAAA";
    private static final String UPDATED_LIKE_DISLIKE = "BBBBB";
    private static final String DEFAULT_SHARE = "AAAAA";
    private static final String UPDATED_SHARE = "BBBBB";
    private static final String DEFAULT_FAVOURITE = "AAAAA";
    private static final String UPDATED_FAVOURITE = "BBBBB";

    private static final Integer DEFAULT_COUNT = 1;
    private static final Integer UPDATED_COUNT = 2;
    private static final String DEFAULT_NOTIFICATION_ID = "AAAAA";
    private static final String UPDATED_NOTIFICATION_ID = "BBBBB";
    private static final String DEFAULT_CONSUMER_ID = "AAAAA";
    private static final String UPDATED_CONSUMER_ID = "BBBBB";

    @Inject
    private ConsumerFeedbackRepository consumerFeedbackRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restConsumerFeedbackMockMvc;

    private ConsumerFeedback consumerFeedback;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ConsumerFeedbackResource consumerFeedbackResource = new ConsumerFeedbackResource();
        ReflectionTestUtils.setField(consumerFeedbackResource, "consumerFeedbackRepository", consumerFeedbackRepository);
        this.restConsumerFeedbackMockMvc = MockMvcBuilders.standaloneSetup(consumerFeedbackResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        consumerFeedbackRepository.deleteAll();
        consumerFeedback = new ConsumerFeedback();
        consumerFeedback.setCreated(DEFAULT_CREATED);
        consumerFeedback.setDeleted(DEFAULT_DELETED);
        consumerFeedback.setLastUpdate(DEFAULT_LAST_UPDATE);
        consumerFeedback.setVersion(DEFAULT_VERSION);
        consumerFeedback.setComment(DEFAULT_COMMENT);
        consumerFeedback.setLikeDislike(DEFAULT_LIKE_DISLIKE);
        consumerFeedback.setShare(DEFAULT_SHARE);
        consumerFeedback.setFavourite(DEFAULT_FAVOURITE);
        consumerFeedback.setCount(DEFAULT_COUNT);
        consumerFeedback.setNotificationId(DEFAULT_NOTIFICATION_ID);
        consumerFeedback.setConsumerId(DEFAULT_CONSUMER_ID);
    }

    @Test
    public void createConsumerFeedback() throws Exception {
        int databaseSizeBeforeCreate = consumerFeedbackRepository.findAll().size();

        // Create the ConsumerFeedback

        restConsumerFeedbackMockMvc.perform(post("/api/consumer-feedbacks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(consumerFeedback)))
                .andExpect(status().isCreated());

        // Validate the ConsumerFeedback in the database
        List<ConsumerFeedback> consumerFeedbacks = consumerFeedbackRepository.findAll();
        assertThat(consumerFeedbacks).hasSize(databaseSizeBeforeCreate + 1);
        ConsumerFeedback testConsumerFeedback = consumerFeedbacks.get(consumerFeedbacks.size() - 1);
        assertThat(testConsumerFeedback.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testConsumerFeedback.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testConsumerFeedback.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testConsumerFeedback.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testConsumerFeedback.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testConsumerFeedback.getLikeDislike()).isEqualTo(DEFAULT_LIKE_DISLIKE);
        assertThat(testConsumerFeedback.getShare()).isEqualTo(DEFAULT_SHARE);
        assertThat(testConsumerFeedback.getFavourite()).isEqualTo(DEFAULT_FAVOURITE);
        assertThat(testConsumerFeedback.getCount()).isEqualTo(DEFAULT_COUNT);
        assertThat(testConsumerFeedback.getNotificationId()).isEqualTo(DEFAULT_NOTIFICATION_ID);
        assertThat(testConsumerFeedback.getConsumerId()).isEqualTo(DEFAULT_CONSUMER_ID);
    }

    @Test
    public void getAllConsumerFeedbacks() throws Exception {
        // Initialize the database
        consumerFeedbackRepository.save(consumerFeedback);

        // Get all the consumerFeedbacks
        restConsumerFeedbackMockMvc.perform(get("/api/consumer-feedbacks?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(consumerFeedback.getId())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED_STR)))
                .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE_STR)))
                .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
                .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
                .andExpect(jsonPath("$.[*].likeDislike").value(hasItem(DEFAULT_LIKE_DISLIKE.toString())))
                .andExpect(jsonPath("$.[*].share").value(hasItem(DEFAULT_SHARE.toString())))
                .andExpect(jsonPath("$.[*].favourite").value(hasItem(DEFAULT_FAVOURITE.toString())))
                .andExpect(jsonPath("$.[*].count").value(hasItem(DEFAULT_COUNT)))
                .andExpect(jsonPath("$.[*].notificationId").value(hasItem(DEFAULT_NOTIFICATION_ID.toString())))
                .andExpect(jsonPath("$.[*].consumerId").value(hasItem(DEFAULT_CONSUMER_ID.toString())));
    }

    @Test
    public void getConsumerFeedback() throws Exception {
        // Initialize the database
        consumerFeedbackRepository.save(consumerFeedback);

        // Get the consumerFeedback
        restConsumerFeedbackMockMvc.perform(get("/api/consumer-feedbacks/{id}", consumerFeedback.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(consumerFeedback.getId()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED_STR))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE_STR))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()))
            .andExpect(jsonPath("$.likeDislike").value(DEFAULT_LIKE_DISLIKE.toString()))
            .andExpect(jsonPath("$.share").value(DEFAULT_SHARE.toString()))
            .andExpect(jsonPath("$.favourite").value(DEFAULT_FAVOURITE.toString()))
            .andExpect(jsonPath("$.count").value(DEFAULT_COUNT))
            .andExpect(jsonPath("$.notificationId").value(DEFAULT_NOTIFICATION_ID.toString()))
            .andExpect(jsonPath("$.consumerId").value(DEFAULT_CONSUMER_ID.toString()));
    }

    @Test
    public void getNonExistingConsumerFeedback() throws Exception {
        // Get the consumerFeedback
        restConsumerFeedbackMockMvc.perform(get("/api/consumer-feedbacks/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateConsumerFeedback() throws Exception {
        // Initialize the database
        consumerFeedbackRepository.save(consumerFeedback);
        int databaseSizeBeforeUpdate = consumerFeedbackRepository.findAll().size();

        // Update the consumerFeedback
        ConsumerFeedback updatedConsumerFeedback = new ConsumerFeedback();
        updatedConsumerFeedback.setId(consumerFeedback.getId());
        updatedConsumerFeedback.setCreated(UPDATED_CREATED);
        updatedConsumerFeedback.setDeleted(UPDATED_DELETED);
        updatedConsumerFeedback.setLastUpdate(UPDATED_LAST_UPDATE);
        updatedConsumerFeedback.setVersion(UPDATED_VERSION);
        updatedConsumerFeedback.setComment(UPDATED_COMMENT);
        updatedConsumerFeedback.setLikeDislike(UPDATED_LIKE_DISLIKE);
        updatedConsumerFeedback.setShare(UPDATED_SHARE);
        updatedConsumerFeedback.setFavourite(UPDATED_FAVOURITE);
        updatedConsumerFeedback.setCount(UPDATED_COUNT);
        updatedConsumerFeedback.setNotificationId(UPDATED_NOTIFICATION_ID);
        updatedConsumerFeedback.setConsumerId(UPDATED_CONSUMER_ID);

        restConsumerFeedbackMockMvc.perform(put("/api/consumer-feedbacks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedConsumerFeedback)))
                .andExpect(status().isOk());

        // Validate the ConsumerFeedback in the database
        List<ConsumerFeedback> consumerFeedbacks = consumerFeedbackRepository.findAll();
        assertThat(consumerFeedbacks).hasSize(databaseSizeBeforeUpdate);
        ConsumerFeedback testConsumerFeedback = consumerFeedbacks.get(consumerFeedbacks.size() - 1);
        assertThat(testConsumerFeedback.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testConsumerFeedback.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testConsumerFeedback.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testConsumerFeedback.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testConsumerFeedback.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testConsumerFeedback.getLikeDislike()).isEqualTo(UPDATED_LIKE_DISLIKE);
        assertThat(testConsumerFeedback.getShare()).isEqualTo(UPDATED_SHARE);
        assertThat(testConsumerFeedback.getFavourite()).isEqualTo(UPDATED_FAVOURITE);
        assertThat(testConsumerFeedback.getCount()).isEqualTo(UPDATED_COUNT);
        assertThat(testConsumerFeedback.getNotificationId()).isEqualTo(UPDATED_NOTIFICATION_ID);
        assertThat(testConsumerFeedback.getConsumerId()).isEqualTo(UPDATED_CONSUMER_ID);
    }

    @Test
    public void deleteConsumerFeedback() throws Exception {
        // Initialize the database
        consumerFeedbackRepository.save(consumerFeedback);
        int databaseSizeBeforeDelete = consumerFeedbackRepository.findAll().size();

        // Get the consumerFeedback
        restConsumerFeedbackMockMvc.perform(delete("/api/consumer-feedbacks/{id}", consumerFeedback.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ConsumerFeedback> consumerFeedbacks = consumerFeedbackRepository.findAll();
        assertThat(consumerFeedbacks).hasSize(databaseSizeBeforeDelete - 1);
    }
}
