(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('NotificationAcknowledgementDetailController', NotificationAcknowledgementDetailController);

    NotificationAcknowledgementDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'NotificationAcknowledgement'];

    function NotificationAcknowledgementDetailController($scope, $rootScope, $stateParams, entity, NotificationAcknowledgement) {
        var vm = this;

        vm.notificationAcknowledgement = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:notificationAcknowledgementUpdate', function(event, result) {
            vm.notificationAcknowledgement = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
