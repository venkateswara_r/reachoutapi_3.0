(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('NotificationAcknowledgementDialogController', NotificationAcknowledgementDialogController);

    NotificationAcknowledgementDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NotificationAcknowledgement'];

    function NotificationAcknowledgementDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NotificationAcknowledgement) {
        var vm = this;

        vm.notificationAcknowledgement = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.notificationAcknowledgement.id !== null) {
                NotificationAcknowledgement.update(vm.notificationAcknowledgement, onSaveSuccess, onSaveError);
            } else {
                NotificationAcknowledgement.save(vm.notificationAcknowledgement, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('reachoutApp:notificationAcknowledgementUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;
        vm.datePickerOpenStatus.deleted = false;
        vm.datePickerOpenStatus.lastUpdate = false;
        vm.datePickerOpenStatus.read = false;
        vm.datePickerOpenStatus.delivered = false;
        vm.datePickerOpenStatus.sent = false;
        vm.datePickerOpenStatus.visited = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
