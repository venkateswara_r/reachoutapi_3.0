(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('NotificationAcknowledgementController', NotificationAcknowledgementController);

    NotificationAcknowledgementController.$inject = ['$scope', '$state', 'NotificationAcknowledgement'];

    function NotificationAcknowledgementController ($scope, $state, NotificationAcknowledgement) {
        var vm = this;
        
        vm.notificationAcknowledgements = [];

        loadAll();

        function loadAll() {
            NotificationAcknowledgement.query(function(result) {
                vm.notificationAcknowledgements = result;
            });
        }
    }
})();
