(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('notification-acknowledgement', {
            parent: 'entity',
            url: '/notification-acknowledgement',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.notificationAcknowledgement.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/notification-acknowledgement/notification-acknowledgements.html',
                    controller: 'NotificationAcknowledgementController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('notificationAcknowledgement');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('notification-acknowledgement-detail', {
            parent: 'entity',
            url: '/notification-acknowledgement/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.notificationAcknowledgement.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/notification-acknowledgement/notification-acknowledgement-detail.html',
                    controller: 'NotificationAcknowledgementDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('notificationAcknowledgement');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'NotificationAcknowledgement', function($stateParams, NotificationAcknowledgement) {
                    return NotificationAcknowledgement.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('notification-acknowledgement.new', {
            parent: 'notification-acknowledgement',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/notification-acknowledgement/notification-acknowledgement-dialog.html',
                    controller: 'NotificationAcknowledgementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                created: null,
                                deleted: null,
                                lastUpdate: null,
                                version: null,
                                read: null,
                                delivered: null,
                                sent: null,
                                visited: null,
                                consumerId: null,
                                notificationId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('notification-acknowledgement', null, { reload: true });
                }, function() {
                    $state.go('notification-acknowledgement');
                });
            }]
        })
        .state('notification-acknowledgement.edit', {
            parent: 'notification-acknowledgement',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/notification-acknowledgement/notification-acknowledgement-dialog.html',
                    controller: 'NotificationAcknowledgementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NotificationAcknowledgement', function(NotificationAcknowledgement) {
                            return NotificationAcknowledgement.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('notification-acknowledgement', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('notification-acknowledgement.delete', {
            parent: 'notification-acknowledgement',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/notification-acknowledgement/notification-acknowledgement-delete-dialog.html',
                    controller: 'NotificationAcknowledgementDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NotificationAcknowledgement', function(NotificationAcknowledgement) {
                            return NotificationAcknowledgement.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('notification-acknowledgement', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
