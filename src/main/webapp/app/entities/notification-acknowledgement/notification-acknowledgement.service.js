(function() {
    'use strict';
    angular
        .module('reachoutApp')
        .factory('NotificationAcknowledgement', NotificationAcknowledgement);

    NotificationAcknowledgement.$inject = ['$resource', 'DateUtils'];

    function NotificationAcknowledgement ($resource, DateUtils) {
        var resourceUrl =  'api/notification-acknowledgements/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.created = DateUtils.convertDateTimeFromServer(data.created);
                        data.deleted = DateUtils.convertDateTimeFromServer(data.deleted);
                        data.lastUpdate = DateUtils.convertDateTimeFromServer(data.lastUpdate);
                        data.read = DateUtils.convertDateTimeFromServer(data.read);
                        data.delivered = DateUtils.convertDateTimeFromServer(data.delivered);
                        data.sent = DateUtils.convertDateTimeFromServer(data.sent);
                        data.visited = DateUtils.convertDateTimeFromServer(data.visited);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
