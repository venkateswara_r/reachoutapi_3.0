(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('NotificationAcknowledgementDeleteController',NotificationAcknowledgementDeleteController);

    NotificationAcknowledgementDeleteController.$inject = ['$uibModalInstance', 'entity', 'NotificationAcknowledgement'];

    function NotificationAcknowledgementDeleteController($uibModalInstance, entity, NotificationAcknowledgement) {
        var vm = this;

        vm.notificationAcknowledgement = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NotificationAcknowledgement.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
