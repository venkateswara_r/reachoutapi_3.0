(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerFavouriteDetailController', ConsumerFavouriteDetailController);

    ConsumerFavouriteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ConsumerFavourite'];

    function ConsumerFavouriteDetailController($scope, $rootScope, $stateParams, entity, ConsumerFavourite) {
        var vm = this;

        vm.consumerFavourite = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:consumerFavouriteUpdate', function(event, result) {
            vm.consumerFavourite = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
