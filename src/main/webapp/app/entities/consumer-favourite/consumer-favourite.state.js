(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('consumer-favourite', {
            parent: 'entity',
            url: '/consumer-favourite',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.consumerFavourite.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consumer-favourite/consumer-favourites.html',
                    controller: 'ConsumerFavouriteController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('consumerFavourite');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('consumer-favourite-detail', {
            parent: 'entity',
            url: '/consumer-favourite/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.consumerFavourite.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consumer-favourite/consumer-favourite-detail.html',
                    controller: 'ConsumerFavouriteDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('consumerFavourite');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ConsumerFavourite', function($stateParams, ConsumerFavourite) {
                    return ConsumerFavourite.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('consumer-favourite.new', {
            parent: 'consumer-favourite',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer-favourite/consumer-favourite-dialog.html',
                    controller: 'ConsumerFavouriteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                created: null,
                                deleted: null,
                                lastUpdate: null,
                                version: null,
                                consumerId: null,
                                providerId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('consumer-favourite', null, { reload: true });
                }, function() {
                    $state.go('consumer-favourite');
                });
            }]
        })
        .state('consumer-favourite.edit', {
            parent: 'consumer-favourite',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer-favourite/consumer-favourite-dialog.html',
                    controller: 'ConsumerFavouriteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ConsumerFavourite', function(ConsumerFavourite) {
                            return ConsumerFavourite.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consumer-favourite', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('consumer-favourite.delete', {
            parent: 'consumer-favourite',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer-favourite/consumer-favourite-delete-dialog.html',
                    controller: 'ConsumerFavouriteDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ConsumerFavourite', function(ConsumerFavourite) {
                            return ConsumerFavourite.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consumer-favourite', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
