(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerFavouriteController', ConsumerFavouriteController);

    ConsumerFavouriteController.$inject = ['$scope', '$state', 'ConsumerFavourite'];

    function ConsumerFavouriteController ($scope, $state, ConsumerFavourite) {
        var vm = this;
        
        vm.consumerFavourites = [];

        loadAll();

        function loadAll() {
            ConsumerFavourite.query(function(result) {
                vm.consumerFavourites = result;
            });
        }
    }
})();
