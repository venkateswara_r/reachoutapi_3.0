(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerFavouriteDeleteController',ConsumerFavouriteDeleteController);

    ConsumerFavouriteDeleteController.$inject = ['$uibModalInstance', 'entity', 'ConsumerFavourite'];

    function ConsumerFavouriteDeleteController($uibModalInstance, entity, ConsumerFavourite) {
        var vm = this;

        vm.consumerFavourite = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ConsumerFavourite.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
