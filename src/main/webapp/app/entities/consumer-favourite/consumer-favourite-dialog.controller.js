(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerFavouriteDialogController', ConsumerFavouriteDialogController);

    ConsumerFavouriteDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ConsumerFavourite'];

    function ConsumerFavouriteDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ConsumerFavourite) {
        var vm = this;

        vm.consumerFavourite = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.consumerFavourite.id !== null) {
                ConsumerFavourite.update(vm.consumerFavourite, onSaveSuccess, onSaveError);
            } else {
                ConsumerFavourite.save(vm.consumerFavourite, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('reachoutApp:consumerFavouriteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;
        vm.datePickerOpenStatus.deleted = false;
        vm.datePickerOpenStatus.lastUpdate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
