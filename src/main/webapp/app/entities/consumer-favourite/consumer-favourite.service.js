(function() {
    'use strict';
    angular
        .module('reachoutApp')
        .factory('ConsumerFavourite', ConsumerFavourite);

    ConsumerFavourite.$inject = ['$resource', 'DateUtils'];

    function ConsumerFavourite ($resource, DateUtils) {
        var resourceUrl =  'api/consumer-favourites/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.created = DateUtils.convertDateTimeFromServer(data.created);
                        data.deleted = DateUtils.convertDateTimeFromServer(data.deleted);
                        data.lastUpdate = DateUtils.convertDateTimeFromServer(data.lastUpdate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
