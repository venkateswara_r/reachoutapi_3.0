(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ContentMetadataDialogController', ContentMetadataDialogController);

    ContentMetadataDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ContentMetadata'];

    function ContentMetadataDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ContentMetadata) {
        var vm = this;

        vm.contentMetadata = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.contentMetadata.id !== null) {
                ContentMetadata.update(vm.contentMetadata, onSaveSuccess, onSaveError);
            } else {
                ContentMetadata.save(vm.contentMetadata, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('reachoutApp:contentMetadataUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;
        vm.datePickerOpenStatus.deleted = false;
        vm.datePickerOpenStatus.lastUpdate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
