(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ContentMetadataDeleteController',ContentMetadataDeleteController);

    ContentMetadataDeleteController.$inject = ['$uibModalInstance', 'entity', 'ContentMetadata'];

    function ContentMetadataDeleteController($uibModalInstance, entity, ContentMetadata) {
        var vm = this;

        vm.contentMetadata = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ContentMetadata.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
