(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('content-metadata', {
            parent: 'entity',
            url: '/content-metadata',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.contentMetadata.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/content-metadata/content-metadata.html',
                    controller: 'ContentMetadataController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contentMetadata');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('content-metadata-detail', {
            parent: 'entity',
            url: '/content-metadata/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.contentMetadata.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/content-metadata/content-metadata-detail.html',
                    controller: 'ContentMetadataDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contentMetadata');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ContentMetadata', function($stateParams, ContentMetadata) {
                    return ContentMetadata.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('content-metadata.new', {
            parent: 'content-metadata',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/content-metadata/content-metadata-dialog.html',
                    controller: 'ContentMetadataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                created: null,
                                deleted: null,
                                lastUpdate: null,
                                version: null,
                                signature: null,
                                format: null,
                                type: null,
                                imageVersion: null,
                                url: null,
                                tags: null,
                                bytes: null,
                                width: null,
                                height: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('content-metadata', null, { reload: true });
                }, function() {
                    $state.go('content-metadata');
                });
            }]
        })
        .state('content-metadata.edit', {
            parent: 'content-metadata',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/content-metadata/content-metadata-dialog.html',
                    controller: 'ContentMetadataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ContentMetadata', function(ContentMetadata) {
                            return ContentMetadata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('content-metadata', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('content-metadata.delete', {
            parent: 'content-metadata',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/content-metadata/content-metadata-delete-dialog.html',
                    controller: 'ContentMetadataDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ContentMetadata', function(ContentMetadata) {
                            return ContentMetadata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('content-metadata', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
