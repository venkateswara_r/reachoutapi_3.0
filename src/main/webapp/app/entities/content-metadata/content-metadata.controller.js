(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ContentMetadataController', ContentMetadataController);

    ContentMetadataController.$inject = ['$scope', '$state', 'ContentMetadata'];

    function ContentMetadataController ($scope, $state, ContentMetadata) {
        var vm = this;
        
        vm.contentMetadata = [];

        loadAll();

        function loadAll() {
            ContentMetadata.query(function(result) {
                vm.contentMetadata = result;
            });
        }
    }
})();
