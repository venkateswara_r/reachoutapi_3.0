(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ContentMetadataDetailController', ContentMetadataDetailController);

    ContentMetadataDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ContentMetadata'];

    function ContentMetadataDetailController($scope, $rootScope, $stateParams, entity, ContentMetadata) {
        var vm = this;

        vm.contentMetadata = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:contentMetadataUpdate', function(event, result) {
            vm.contentMetadata = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
