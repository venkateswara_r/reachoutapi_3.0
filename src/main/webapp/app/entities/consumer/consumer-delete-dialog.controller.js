(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerDeleteController',ConsumerDeleteController);

    ConsumerDeleteController.$inject = ['$uibModalInstance', 'entity', 'Consumer'];

    function ConsumerDeleteController($uibModalInstance, entity, Consumer) {
        var vm = this;

        vm.consumer = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Consumer.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
