(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerDialogController', ConsumerDialogController);

    ConsumerDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Consumer'];

    function ConsumerDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Consumer) {
        var vm = this;

        vm.consumer = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.consumer.id !== null) {
                Consumer.update(vm.consumer, onSaveSuccess, onSaveError);
            } else {
                Consumer.save(vm.consumer, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('reachoutApp:consumerUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;
        vm.datePickerOpenStatus.deleted = false;
        vm.datePickerOpenStatus.lastUpdate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
