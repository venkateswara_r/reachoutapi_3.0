(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('consumer', {
            parent: 'entity',
            url: '/consumer',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.consumer.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consumer/consumers.html',
                    controller: 'ConsumerController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('consumer');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('consumer-detail', {
            parent: 'entity',
            url: '/consumer/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.consumer.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consumer/consumer-detail.html',
                    controller: 'ConsumerDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('consumer');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Consumer', function($stateParams, Consumer) {
                    return Consumer.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('consumer.new', {
            parent: 'consumer',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer/consumer-dialog.html',
                    controller: 'ConsumerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                created: null,
                                deleted: null,
                                lastUpdate: null,
                                version: null,
                                name: null,
                                mobile: null,
                                email: null,
                                status: null,
                                active: null,
                                otp: null,
                                otpCount: null,
                                deviceInfo: null,
                                location: null,
                                unsubscribeCategory: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('consumer', null, { reload: true });
                }, function() {
                    $state.go('consumer');
                });
            }]
        })
        .state('consumer.edit', {
            parent: 'consumer',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer/consumer-dialog.html',
                    controller: 'ConsumerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Consumer', function(Consumer) {
                            return Consumer.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consumer', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('consumer.delete', {
            parent: 'consumer',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer/consumer-delete-dialog.html',
                    controller: 'ConsumerDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Consumer', function(Consumer) {
                            return Consumer.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consumer', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
