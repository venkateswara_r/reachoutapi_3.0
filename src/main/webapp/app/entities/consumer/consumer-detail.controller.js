(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerDetailController', ConsumerDetailController);

    ConsumerDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Consumer'];

    function ConsumerDetailController($scope, $rootScope, $stateParams, entity, Consumer) {
        var vm = this;

        vm.consumer = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:consumerUpdate', function(event, result) {
            vm.consumer = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
