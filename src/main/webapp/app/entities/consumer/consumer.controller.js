(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerController', ConsumerController);

    ConsumerController.$inject = ['$scope', '$state', 'Consumer'];

    function ConsumerController ($scope, $state, Consumer) {
        var vm = this;
        
        vm.consumers = [];

        loadAll();

        function loadAll() {
            Consumer.query(function(result) {
                vm.consumers = result;
            });
        }
    }
})();
