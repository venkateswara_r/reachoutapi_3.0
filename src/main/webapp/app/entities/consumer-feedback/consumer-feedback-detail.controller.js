(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerFeedbackDetailController', ConsumerFeedbackDetailController);

    ConsumerFeedbackDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ConsumerFeedback'];

    function ConsumerFeedbackDetailController($scope, $rootScope, $stateParams, entity, ConsumerFeedback) {
        var vm = this;

        vm.consumerFeedback = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:consumerFeedbackUpdate', function(event, result) {
            vm.consumerFeedback = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
