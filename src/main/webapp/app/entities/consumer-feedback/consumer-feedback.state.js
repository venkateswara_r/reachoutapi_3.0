(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('consumer-feedback', {
            parent: 'entity',
            url: '/consumer-feedback',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.consumerFeedback.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consumer-feedback/consumer-feedbacks.html',
                    controller: 'ConsumerFeedbackController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('consumerFeedback');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('consumer-feedback-detail', {
            parent: 'entity',
            url: '/consumer-feedback/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.consumerFeedback.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consumer-feedback/consumer-feedback-detail.html',
                    controller: 'ConsumerFeedbackDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('consumerFeedback');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ConsumerFeedback', function($stateParams, ConsumerFeedback) {
                    return ConsumerFeedback.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('consumer-feedback.new', {
            parent: 'consumer-feedback',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer-feedback/consumer-feedback-dialog.html',
                    controller: 'ConsumerFeedbackDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                created: null,
                                deleted: null,
                                lastUpdate: null,
                                version: null,
                                comment: null,
                                likeDislike: null,
                                share: null,
                                favourite: null,
                                count: null,
                                notificationId: null,
                                consumerId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('consumer-feedback', null, { reload: true });
                }, function() {
                    $state.go('consumer-feedback');
                });
            }]
        })
        .state('consumer-feedback.edit', {
            parent: 'consumer-feedback',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer-feedback/consumer-feedback-dialog.html',
                    controller: 'ConsumerFeedbackDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ConsumerFeedback', function(ConsumerFeedback) {
                            return ConsumerFeedback.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consumer-feedback', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('consumer-feedback.delete', {
            parent: 'consumer-feedback',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer-feedback/consumer-feedback-delete-dialog.html',
                    controller: 'ConsumerFeedbackDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ConsumerFeedback', function(ConsumerFeedback) {
                            return ConsumerFeedback.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consumer-feedback', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
