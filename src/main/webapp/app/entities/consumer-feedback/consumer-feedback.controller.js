(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerFeedbackController', ConsumerFeedbackController);

    ConsumerFeedbackController.$inject = ['$scope', '$state', 'ConsumerFeedback'];

    function ConsumerFeedbackController ($scope, $state, ConsumerFeedback) {
        var vm = this;
        
        vm.consumerFeedbacks = [];

        loadAll();

        function loadAll() {
            ConsumerFeedback.query(function(result) {
                vm.consumerFeedbacks = result;
            });
        }
    }
})();
