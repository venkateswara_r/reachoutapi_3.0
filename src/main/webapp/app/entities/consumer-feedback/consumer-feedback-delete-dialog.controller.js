(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerFeedbackDeleteController',ConsumerFeedbackDeleteController);

    ConsumerFeedbackDeleteController.$inject = ['$uibModalInstance', 'entity', 'ConsumerFeedback'];

    function ConsumerFeedbackDeleteController($uibModalInstance, entity, ConsumerFeedback) {
        var vm = this;

        vm.consumerFeedback = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ConsumerFeedback.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
