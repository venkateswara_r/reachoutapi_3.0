(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerFeedbackDialogController', ConsumerFeedbackDialogController);

    ConsumerFeedbackDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ConsumerFeedback'];

    function ConsumerFeedbackDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ConsumerFeedback) {
        var vm = this;

        vm.consumerFeedback = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.consumerFeedback.id !== null) {
                ConsumerFeedback.update(vm.consumerFeedback, onSaveSuccess, onSaveError);
            } else {
                ConsumerFeedback.save(vm.consumerFeedback, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('reachoutApp:consumerFeedbackUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;
        vm.datePickerOpenStatus.deleted = false;
        vm.datePickerOpenStatus.lastUpdate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
