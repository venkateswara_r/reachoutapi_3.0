(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('DeviceInfoDetailController', DeviceInfoDetailController);

    DeviceInfoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'DeviceInfo'];

    function DeviceInfoDetailController($scope, $rootScope, $stateParams, entity, DeviceInfo) {
        var vm = this;

        vm.deviceInfo = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:deviceInfoUpdate', function(event, result) {
            vm.deviceInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
