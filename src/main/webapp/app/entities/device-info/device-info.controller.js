(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('DeviceInfoController', DeviceInfoController);

    DeviceInfoController.$inject = ['$scope', '$state', 'DeviceInfo'];

    function DeviceInfoController ($scope, $state, DeviceInfo) {
        var vm = this;
        
        vm.deviceInfos = [];

        loadAll();

        function loadAll() {
            DeviceInfo.query(function(result) {
                vm.deviceInfos = result;
            });
        }
    }
})();
