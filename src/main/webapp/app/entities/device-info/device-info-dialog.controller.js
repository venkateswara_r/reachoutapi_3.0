(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('DeviceInfoDialogController', DeviceInfoDialogController);

    DeviceInfoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DeviceInfo'];

    function DeviceInfoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DeviceInfo) {
        var vm = this;

        vm.deviceInfo = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.deviceInfo.id !== null) {
                DeviceInfo.update(vm.deviceInfo, onSaveSuccess, onSaveError);
            } else {
                DeviceInfo.save(vm.deviceInfo, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('reachoutApp:deviceInfoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;
        vm.datePickerOpenStatus.deleted = false;
        vm.datePickerOpenStatus.lastUpdate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
