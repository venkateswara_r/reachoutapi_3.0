(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('device-info', {
            parent: 'entity',
            url: '/device-info',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.deviceInfo.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/device-info/device-infos.html',
                    controller: 'DeviceInfoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('deviceInfo');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('device-info-detail', {
            parent: 'entity',
            url: '/device-info/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.deviceInfo.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/device-info/device-info-detail.html',
                    controller: 'DeviceInfoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('deviceInfo');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DeviceInfo', function($stateParams, DeviceInfo) {
                    return DeviceInfo.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('device-info.new', {
            parent: 'device-info',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/device-info/device-info-dialog.html',
                    controller: 'DeviceInfoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                created: null,
                                deleted: null,
                                lastUpdate: null,
                                version: null,
                                device: null,
                                sdk: null,
                                model: null,
                                product: null,
                                deviceId: null,
                                consumerId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('device-info', null, { reload: true });
                }, function() {
                    $state.go('device-info');
                });
            }]
        })
        .state('device-info.edit', {
            parent: 'device-info',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/device-info/device-info-dialog.html',
                    controller: 'DeviceInfoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DeviceInfo', function(DeviceInfo) {
                            return DeviceInfo.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('device-info', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('device-info.delete', {
            parent: 'device-info',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/device-info/device-info-delete-dialog.html',
                    controller: 'DeviceInfoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DeviceInfo', function(DeviceInfo) {
                            return DeviceInfo.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('device-info', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
