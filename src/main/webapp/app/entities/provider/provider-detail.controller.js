(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ProviderDetailController', ProviderDetailController);

    ProviderDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Provider'];

    function ProviderDetailController($scope, $rootScope, $stateParams, entity, Provider) {
        var vm = this;

        vm.provider = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:providerUpdate', function(event, result) {
            vm.provider = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
