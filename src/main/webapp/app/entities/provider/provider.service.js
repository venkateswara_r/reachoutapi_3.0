(function() {
    'use strict';
    angular
        .module('reachoutApp')
        .factory('Provider', Provider);

    Provider.$inject = ['$resource', 'DateUtils'];

    function Provider ($resource, DateUtils) {
        var resourceUrl =  'api/providers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.created = DateUtils.convertDateTimeFromServer(data.created);
                        data.deleted = DateUtils.convertDateTimeFromServer(data.deleted);
                        data.lastUpdate = DateUtils.convertDateTimeFromServer(data.lastUpdate);
                        data.promoCreatedDate = DateUtils.convertDateTimeFromServer(data.promoCreatedDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
