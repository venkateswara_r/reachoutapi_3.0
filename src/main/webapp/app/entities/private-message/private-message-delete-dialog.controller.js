(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('PrivateMessageDeleteController',PrivateMessageDeleteController);

    PrivateMessageDeleteController.$inject = ['$uibModalInstance', 'entity', 'PrivateMessage'];

    function PrivateMessageDeleteController($uibModalInstance, entity, PrivateMessage) {
        var vm = this;

        vm.privateMessage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PrivateMessage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
