(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('private-message', {
            parent: 'entity',
            url: '/private-message',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.privateMessage.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/private-message/private-messages.html',
                    controller: 'PrivateMessageController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('privateMessage');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('private-message-detail', {
            parent: 'entity',
            url: '/private-message/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.privateMessage.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/private-message/private-message-detail.html',
                    controller: 'PrivateMessageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('privateMessage');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PrivateMessage', function($stateParams, PrivateMessage) {
                    return PrivateMessage.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('private-message.new', {
            parent: 'private-message',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/private-message/private-message-dialog.html',
                    controller: 'PrivateMessageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                created: null,
                                deleted: null,
                                lastUpdate: null,
                                version: null,
                                message: null,
                                read: null,
                                delivered: null,
                                notificationId: null,
                                senderId: null,
                                receiverId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('private-message', null, { reload: true });
                }, function() {
                    $state.go('private-message');
                });
            }]
        })
        .state('private-message.edit', {
            parent: 'private-message',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/private-message/private-message-dialog.html',
                    controller: 'PrivateMessageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PrivateMessage', function(PrivateMessage) {
                            return PrivateMessage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('private-message', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('private-message.delete', {
            parent: 'private-message',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/private-message/private-message-delete-dialog.html',
                    controller: 'PrivateMessageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PrivateMessage', function(PrivateMessage) {
                            return PrivateMessage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('private-message', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
