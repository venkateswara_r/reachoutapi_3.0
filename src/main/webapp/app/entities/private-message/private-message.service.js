(function() {
    'use strict';
    angular
        .module('reachoutApp')
        .factory('PrivateMessage', PrivateMessage);

    PrivateMessage.$inject = ['$resource', 'DateUtils'];

    function PrivateMessage ($resource, DateUtils) {
        var resourceUrl =  'api/private-messages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.created = DateUtils.convertDateTimeFromServer(data.created);
                        data.deleted = DateUtils.convertDateTimeFromServer(data.deleted);
                        data.lastUpdate = DateUtils.convertDateTimeFromServer(data.lastUpdate);
                        data.read = DateUtils.convertDateTimeFromServer(data.read);
                        data.delivered = DateUtils.convertDateTimeFromServer(data.delivered);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
