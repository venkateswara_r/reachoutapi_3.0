(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('PrivateMessageDetailController', PrivateMessageDetailController);

    PrivateMessageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'PrivateMessage'];

    function PrivateMessageDetailController($scope, $rootScope, $stateParams, entity, PrivateMessage) {
        var vm = this;

        vm.privateMessage = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:privateMessageUpdate', function(event, result) {
            vm.privateMessage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
