(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('PrivateMessageController', PrivateMessageController);

    PrivateMessageController.$inject = ['$scope', '$state', 'PrivateMessage'];

    function PrivateMessageController ($scope, $state, PrivateMessage) {
        var vm = this;
        
        vm.privateMessages = [];

        loadAll();

        function loadAll() {
            PrivateMessage.query(function(result) {
                vm.privateMessages = result;
            });
        }
    }
})();
