(function() {
    'use strict';
    angular
        .module('reachoutApp')
        .factory('Notification', Notification);

    Notification.$inject = ['$resource', 'DateUtils'];

    function Notification ($resource, DateUtils) {
        var resourceUrl =  'api/notifications/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.created = DateUtils.convertDateTimeFromServer(data.created);
                        data.deleted = DateUtils.convertDateTimeFromServer(data.deleted);
                        data.lastUpdate = DateUtils.convertDateTimeFromServer(data.lastUpdate);
                        data.approved = DateUtils.convertDateTimeFromServer(data.approved);
                        data.validFrom = DateUtils.convertDateTimeFromServer(data.validFrom);
                        data.validTo = DateUtils.convertDateTimeFromServer(data.validTo);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
