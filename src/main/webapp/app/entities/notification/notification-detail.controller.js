(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('NotificationDetailController', NotificationDetailController);

    NotificationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Notification'];

    function NotificationDetailController($scope, $rootScope, $stateParams, entity, Notification) {
        var vm = this;

        vm.notification = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:notificationUpdate', function(event, result) {
            vm.notification = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
