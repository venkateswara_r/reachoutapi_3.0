(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ApplicationSettingsDeleteController',ApplicationSettingsDeleteController);

    ApplicationSettingsDeleteController.$inject = ['$uibModalInstance', 'entity', 'ApplicationSettings'];

    function ApplicationSettingsDeleteController($uibModalInstance, entity, ApplicationSettings) {
        var vm = this;

        vm.applicationSettings = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ApplicationSettings.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
