(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ApplicationSettingsDetailController', ApplicationSettingsDetailController);

    ApplicationSettingsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ApplicationSettings'];

    function ApplicationSettingsDetailController($scope, $rootScope, $stateParams, entity, ApplicationSettings) {
        var vm = this;

        vm.applicationSettings = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:applicationSettingsUpdate', function(event, result) {
            vm.applicationSettings = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
