(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ApplicationSettingsController', ApplicationSettingsController);

    ApplicationSettingsController.$inject = ['$scope', '$state', 'ApplicationSettings'];

    function ApplicationSettingsController ($scope, $state, ApplicationSettings) {
        var vm = this;
        
        vm.applicationSettings = [];

        loadAll();

        function loadAll() {
            ApplicationSettings.query(function(result) {
                vm.applicationSettings = result;
            });
        }
    }
})();
