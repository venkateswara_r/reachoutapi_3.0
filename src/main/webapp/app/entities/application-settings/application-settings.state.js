(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('application-settings', {
            parent: 'entity',
            url: '/application-settings',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.applicationSettings.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/application-settings/application-settings.html',
                    controller: 'ApplicationSettingsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('applicationSettings');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('application-settings-detail', {
            parent: 'entity',
            url: '/application-settings/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.applicationSettings.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/application-settings/application-settings-detail.html',
                    controller: 'ApplicationSettingsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('applicationSettings');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ApplicationSettings', function($stateParams, ApplicationSettings) {
                    return ApplicationSettings.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('application-settings.new', {
            parent: 'application-settings',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/application-settings/application-settings-dialog.html',
                    controller: 'ApplicationSettingsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                created: null,
                                deleted: null,
                                lastUpdate: null,
                                version: null,
                                name: null,
                                value: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('application-settings', null, { reload: true });
                }, function() {
                    $state.go('application-settings');
                });
            }]
        })
        .state('application-settings.edit', {
            parent: 'application-settings',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/application-settings/application-settings-dialog.html',
                    controller: 'ApplicationSettingsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ApplicationSettings', function(ApplicationSettings) {
                            return ApplicationSettings.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('application-settings', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('application-settings.delete', {
            parent: 'application-settings',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/application-settings/application-settings-delete-dialog.html',
                    controller: 'ApplicationSettingsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ApplicationSettings', function(ApplicationSettings) {
                            return ApplicationSettings.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('application-settings', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
