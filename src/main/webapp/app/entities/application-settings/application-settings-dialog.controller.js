(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ApplicationSettingsDialogController', ApplicationSettingsDialogController);

    ApplicationSettingsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ApplicationSettings'];

    function ApplicationSettingsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ApplicationSettings) {
        var vm = this;

        vm.applicationSettings = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.applicationSettings.id !== null) {
                ApplicationSettings.update(vm.applicationSettings, onSaveSuccess, onSaveError);
            } else {
                ApplicationSettings.save(vm.applicationSettings, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('reachoutApp:applicationSettingsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;
        vm.datePickerOpenStatus.deleted = false;
        vm.datePickerOpenStatus.lastUpdate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
