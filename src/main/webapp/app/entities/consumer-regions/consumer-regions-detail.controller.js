(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerRegionsDetailController', ConsumerRegionsDetailController);

    ConsumerRegionsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ConsumerRegions'];

    function ConsumerRegionsDetailController($scope, $rootScope, $stateParams, entity, ConsumerRegions) {
        var vm = this;

        vm.consumerRegions = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:consumerRegionsUpdate', function(event, result) {
            vm.consumerRegions = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
