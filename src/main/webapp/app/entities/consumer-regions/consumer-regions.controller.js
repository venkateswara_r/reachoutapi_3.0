(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerRegionsController', ConsumerRegionsController);

    ConsumerRegionsController.$inject = ['$scope', '$state', 'ConsumerRegions'];

    function ConsumerRegionsController ($scope, $state, ConsumerRegions) {
        var vm = this;
        
        vm.consumerRegions = [];

        loadAll();

        function loadAll() {
            ConsumerRegions.query(function(result) {
                vm.consumerRegions = result;
            });
        }
    }
})();
