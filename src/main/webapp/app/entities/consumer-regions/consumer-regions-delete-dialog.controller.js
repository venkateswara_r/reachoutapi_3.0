(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerRegionsDeleteController',ConsumerRegionsDeleteController);

    ConsumerRegionsDeleteController.$inject = ['$uibModalInstance', 'entity', 'ConsumerRegions'];

    function ConsumerRegionsDeleteController($uibModalInstance, entity, ConsumerRegions) {
        var vm = this;

        vm.consumerRegions = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ConsumerRegions.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
