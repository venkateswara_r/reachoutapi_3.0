(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('consumer-regions', {
            parent: 'entity',
            url: '/consumer-regions',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.consumerRegions.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consumer-regions/consumer-regions.html',
                    controller: 'ConsumerRegionsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('consumerRegions');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('consumer-regions-detail', {
            parent: 'entity',
            url: '/consumer-regions/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'reachoutApp.consumerRegions.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/consumer-regions/consumer-regions-detail.html',
                    controller: 'ConsumerRegionsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('consumerRegions');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ConsumerRegions', function($stateParams, ConsumerRegions) {
                    return ConsumerRegions.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('consumer-regions.new', {
            parent: 'consumer-regions',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer-regions/consumer-regions-dialog.html',
                    controller: 'ConsumerRegionsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                created: null,
                                deleted: null,
                                lastUpdate: null,
                                version: null,
                                consumerId: null,
                                region: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('consumer-regions', null, { reload: true });
                }, function() {
                    $state.go('consumer-regions');
                });
            }]
        })
        .state('consumer-regions.edit', {
            parent: 'consumer-regions',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer-regions/consumer-regions-dialog.html',
                    controller: 'ConsumerRegionsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ConsumerRegions', function(ConsumerRegions) {
                            return ConsumerRegions.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consumer-regions', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('consumer-regions.delete', {
            parent: 'consumer-regions',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/consumer-regions/consumer-regions-delete-dialog.html',
                    controller: 'ConsumerRegionsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ConsumerRegions', function(ConsumerRegions) {
                            return ConsumerRegions.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('consumer-regions', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
