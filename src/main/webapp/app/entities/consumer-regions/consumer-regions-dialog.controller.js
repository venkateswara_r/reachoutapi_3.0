(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('ConsumerRegionsDialogController', ConsumerRegionsDialogController);

    ConsumerRegionsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ConsumerRegions'];

    function ConsumerRegionsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ConsumerRegions) {
        var vm = this;

        vm.consumerRegions = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.consumerRegions.id !== null) {
                ConsumerRegions.update(vm.consumerRegions, onSaveSuccess, onSaveError);
            } else {
                ConsumerRegions.save(vm.consumerRegions, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('reachoutApp:consumerRegionsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;
        vm.datePickerOpenStatus.deleted = false;
        vm.datePickerOpenStatus.lastUpdate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
