(function() {
    'use strict';
    angular
        .module('reachoutApp')
        .factory('ConsumerRegions', ConsumerRegions);

    ConsumerRegions.$inject = ['$resource', 'DateUtils'];

    function ConsumerRegions ($resource, DateUtils) {
        var resourceUrl =  'api/consumer-regions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.created = DateUtils.convertDateTimeFromServer(data.created);
                        data.deleted = DateUtils.convertDateTimeFromServer(data.deleted);
                        data.lastUpdate = DateUtils.convertDateTimeFromServer(data.lastUpdate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
