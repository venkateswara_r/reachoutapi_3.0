(function() {
    'use strict';
    angular
        .module('reachoutApp')
        .factory('Transaction', Transaction);

    Transaction.$inject = ['$resource', 'DateUtils'];

    function Transaction ($resource, DateUtils) {
        var resourceUrl =  'api/transactions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.created = DateUtils.convertDateTimeFromServer(data.created);
                        data.deleted = DateUtils.convertDateTimeFromServer(data.deleted);
                        data.lastUpdate = DateUtils.convertDateTimeFromServer(data.lastUpdate);
                        data.transactionDate = DateUtils.convertDateTimeFromServer(data.transactionDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
