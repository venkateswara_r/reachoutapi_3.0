(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('TransactionDetailController', TransactionDetailController);

    TransactionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Transaction'];

    function TransactionDetailController($scope, $rootScope, $stateParams, entity, Transaction) {
        var vm = this;

        vm.transaction = entity;

        var unsubscribe = $rootScope.$on('reachoutApp:transactionUpdate', function(event, result) {
            vm.transaction = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
