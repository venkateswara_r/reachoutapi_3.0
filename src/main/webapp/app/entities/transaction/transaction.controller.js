(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .controller('TransactionController', TransactionController);

    TransactionController.$inject = ['$scope', '$state', 'Transaction'];

    function TransactionController ($scope, $state, Transaction) {
        var vm = this;
        
        vm.transactions = [];

        loadAll();

        function loadAll() {
            Transaction.query(function(result) {
                vm.transactions = result;
            });
        }
    }
})();
