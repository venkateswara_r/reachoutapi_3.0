(function() {
    'use strict';

    angular
        .module('reachoutApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
