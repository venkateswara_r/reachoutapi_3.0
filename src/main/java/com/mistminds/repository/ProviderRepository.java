package com.mistminds.repository;

import com.mistminds.domain.Provider;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Provider entity.
 */
public interface ProviderRepository extends MongoRepository<Provider,String> {

}
