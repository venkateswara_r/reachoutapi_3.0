package com.mistminds.repository;

import com.mistminds.domain.Consumer;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Consumer entity.
 */
public interface ConsumerRepository extends MongoRepository<Consumer,String> {

}
