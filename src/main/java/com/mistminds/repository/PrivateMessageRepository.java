package com.mistminds.repository;

import com.mistminds.domain.PrivateMessage;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the PrivateMessage entity.
 */
public interface PrivateMessageRepository extends MongoRepository<PrivateMessage,String> {

}
