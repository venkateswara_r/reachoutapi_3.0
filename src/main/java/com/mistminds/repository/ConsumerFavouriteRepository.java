package com.mistminds.repository;

import com.mistminds.domain.ConsumerFavourite;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the ConsumerFavourite entity.
 */
public interface ConsumerFavouriteRepository extends MongoRepository<ConsumerFavourite,String> {

}
