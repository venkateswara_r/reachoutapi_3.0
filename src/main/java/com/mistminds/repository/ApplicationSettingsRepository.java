package com.mistminds.repository;

import com.mistminds.domain.ApplicationSettings;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the ApplicationSettings entity.
 */
public interface ApplicationSettingsRepository extends MongoRepository<ApplicationSettings,String> {

}
