package com.mistminds.repository;

import com.mistminds.domain.Transaction;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Transaction entity.
 */
public interface TransactionRepository extends MongoRepository<Transaction,String> {

}
