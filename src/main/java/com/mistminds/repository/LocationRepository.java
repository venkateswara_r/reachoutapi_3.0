package com.mistminds.repository;

import com.mistminds.domain.Location;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Location entity.
 */
public interface LocationRepository extends MongoRepository<Location,String> {

}
