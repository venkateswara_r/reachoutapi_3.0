package com.mistminds.repository;

import com.mistminds.domain.Notification;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Notification entity.
 */
public interface NotificationRepository extends MongoRepository<Notification,String> {

}
