package com.mistminds.repository;

import com.mistminds.domain.Region;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Region entity.
 */
public interface RegionRepository extends MongoRepository<Region,String> {

}
