package com.mistminds.repository;

import com.mistminds.domain.ConsumerFeedback;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the ConsumerFeedback entity.
 */
public interface ConsumerFeedbackRepository extends MongoRepository<ConsumerFeedback,String> {

}
