package com.mistminds.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Transaction.
 */

@Document(collection = "transaction")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("created")
    @CreatedDate
    @JsonIgnore
    private ZonedDateTime created;

    @Field("deleted")
    private ZonedDateTime deleted;

    @Field("last_update")
    @LastModifiedDate
    @JsonIgnore
    private ZonedDateTime lastUpdate;

    @Field("version")
    private Integer version;

    @Field("order_id")
    private String orderId;

    @Field("tracking_id")
    private String trackingId;

    @Field("bank_reference_number")
    private String bankReferenceNumber;

    @Field("order_status")
    private String orderStatus;

    @Field("failure_message")
    private String failureMessage;

    @Field("payment_mode")
    private String paymentMode;

    @Field("card_name")
    private String cardName;

    @Field("status_code")
    private String statusCode;

    @Field("status_message")
    private String statusMessage;

    @Field("amount")
    private Double amount;

    @Field("provider_id")
    private String providerId;

    @Field("vault")
    private String vault;

    @Field("offer_type")
    private String offerType;

    @Field("offer_code")
    private String offerCode;

    @Field("discount_value")
    private String discountValue;

    @Field("merchant_amount")
    private Double merchantAmount;

    @Field("eci_value")
    private String eciValue;

    @Field("retry")
    private Boolean retry;

    @Field("transaction_date")
    private ZonedDateTime transactionDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(ZonedDateTime deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getBankReferenceNumber() {
        return bankReferenceNumber;
    }

    public void setBankReferenceNumber(String bankReferenceNumber) {
        this.bankReferenceNumber = bankReferenceNumber;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getVault() {
        return vault;
    }

    public void setVault(String vault) {
        this.vault = vault;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getOfferCode() {
        return offerCode;
    }

    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    public Double getMerchantAmount() {
        return merchantAmount;
    }

    public void setMerchantAmount(Double merchantAmount) {
        this.merchantAmount = merchantAmount;
    }

    public String getEciValue() {
        return eciValue;
    }

    public void setEciValue(String eciValue) {
        this.eciValue = eciValue;
    }

    public Boolean isRetry() {
        return retry;
    }

    public void setRetry(Boolean retry) {
        this.retry = retry;
    }

    public ZonedDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(ZonedDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transaction transaction = (Transaction) o;
        if(transaction.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, transaction.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + id +
            ", created='" + created + "'" +
            ", deleted='" + deleted + "'" +
            ", lastUpdate='" + lastUpdate + "'" +
            ", version='" + version + "'" +
            ", orderId='" + orderId + "'" +
            ", trackingId='" + trackingId + "'" +
            ", bankReferenceNumber='" + bankReferenceNumber + "'" +
            ", orderStatus='" + orderStatus + "'" +
            ", failureMessage='" + failureMessage + "'" +
            ", paymentMode='" + paymentMode + "'" +
            ", cardName='" + cardName + "'" +
            ", statusCode='" + statusCode + "'" +
            ", statusMessage='" + statusMessage + "'" +
            ", amount='" + amount + "'" +
            ", providerId='" + providerId + "'" +
            ", vault='" + vault + "'" +
            ", offerType='" + offerType + "'" +
            ", offerCode='" + offerCode + "'" +
            ", discountValue='" + discountValue + "'" +
            ", merchantAmount='" + merchantAmount + "'" +
            ", eciValue='" + eciValue + "'" +
            ", retry='" + retry + "'" +
            ", transactionDate='" + transactionDate + "'" +
            '}';
    }
}
