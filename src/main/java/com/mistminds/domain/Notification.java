package com.mistminds.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Notification.
 */

@Document(collection = "notification")
public class Notification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("created")
    @CreatedDate
    @JsonIgnore
    private ZonedDateTime created;

    @Field("deleted")
    private ZonedDateTime deleted;

    @Field("last_update")
    @LastModifiedDate
    @JsonIgnore
    private ZonedDateTime lastUpdate;

    @Field("version")
    private Integer version;

    @Field("category")
    private String category;

    @Field("title")
    private String title;

    @Field("description")
    private String description;

    @Field("offensive")
    private Boolean offensive;

    @Field("image_url")
    private String imageUrl;

    @Field("allow_calls")
    private Boolean allowCalls;

    @Field("show_location")
    private Boolean showLocation;

    @Field("approved")
    private ZonedDateTime approved;

    @Field("radius")
    private Integer radius;

    @Field("free_credits_used")
    private Integer freeCreditsUsed;

    @Field("wallet_credits_used")
    private Integer walletCreditsUsed;

    @Field("valid_from")
    private ZonedDateTime validFrom;

    @Field("valid_to")
    private ZonedDateTime validTo;

    @Field("consumer_id")
    private String consumerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(ZonedDateTime deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isOffensive() {
        return offensive;
    }

    public void setOffensive(Boolean offensive) {
        this.offensive = offensive;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean isAllowCalls() {
        return allowCalls;
    }

    public void setAllowCalls(Boolean allowCalls) {
        this.allowCalls = allowCalls;
    }

    public Boolean isShowLocation() {
        return showLocation;
    }

    public void setShowLocation(Boolean showLocation) {
        this.showLocation = showLocation;
    }

    public ZonedDateTime getApproved() {
        return approved;
    }

    public void setApproved(ZonedDateTime approved) {
        this.approved = approved;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Integer getFreeCreditsUsed() {
        return freeCreditsUsed;
    }

    public void setFreeCreditsUsed(Integer freeCreditsUsed) {
        this.freeCreditsUsed = freeCreditsUsed;
    }

    public Integer getWalletCreditsUsed() {
        return walletCreditsUsed;
    }

    public void setWalletCreditsUsed(Integer walletCreditsUsed) {
        this.walletCreditsUsed = walletCreditsUsed;
    }

    public ZonedDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(ZonedDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public ZonedDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(ZonedDateTime validTo) {
        this.validTo = validTo;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Notification notification = (Notification) o;
        if(notification.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, notification.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Notification{" +
            "id=" + id +
            ", created='" + created + "'" +
            ", deleted='" + deleted + "'" +
            ", lastUpdate='" + lastUpdate + "'" +
            ", version='" + version + "'" +
            ", category='" + category + "'" +
            ", title='" + title + "'" +
            ", description='" + description + "'" +
            ", offensive='" + offensive + "'" +
            ", imageUrl='" + imageUrl + "'" +
            ", allowCalls='" + allowCalls + "'" +
            ", showLocation='" + showLocation + "'" +
            ", approved='" + approved + "'" +
            ", radius='" + radius + "'" +
            ", freeCreditsUsed='" + freeCreditsUsed + "'" +
            ", walletCreditsUsed='" + walletCreditsUsed + "'" +
            ", validFrom='" + validFrom + "'" +
            ", validTo='" + validTo + "'" +
            ", consumerId='" + consumerId + "'" +
            '}';
    }
}
