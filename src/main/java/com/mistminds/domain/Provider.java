package com.mistminds.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Version;

/**
 * A Provider.
 */

@Document(collection = "provider")
public class Provider implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("created")
    @CreatedDate
    @JsonIgnore
    private ZonedDateTime created;

    @Field("deleted")
    private ZonedDateTime deleted;

    @Field("last_update")
    @LastModifiedDate
    @JsonIgnore
    private ZonedDateTime lastUpdate;

    @Field("version")
    private Integer version;

    @Field("name")
    private String name;

    @Field("mobile")
    private String mobile;

    @Field("email")
    private String email;

    @Field("address")
    private String address;

    @Field("image_url")
    private String imageUrl;

    @Field("promo_created_date")
    private ZonedDateTime promoCreatedDate;

    @Field("monthly_free_credits")
    private Integer monthlyFreeCredits;

    @Field("wallet_credits")
    private Integer walletCredits;

    @Field("eligible_for_promo_credit")
    private Boolean eligibleForPromoCredit;

    @Field("active")
    private Boolean active;

    @Field("location")
    private List<Double> location;

    @Field("consumer_id")
    private String consumerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(nullable=false)
    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getDeleted() {
        return deleted;
    }

    @Column(nullable=false)
    public void setDeleted(ZonedDateTime deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Version
    @Column(nullable=false)
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ZonedDateTime getPromoCreatedDate() {
        return promoCreatedDate;
    }

    public void setPromoCreatedDate(ZonedDateTime promoCreatedDate) {
        this.promoCreatedDate = promoCreatedDate;
    }

    public Integer getMonthlyFreeCredits() {
        return monthlyFreeCredits;
    }

    public void setMonthlyFreeCredits(Integer monthlyFreeCredits) {
        this.monthlyFreeCredits = monthlyFreeCredits;
    }

    public Integer getWalletCredits() {
        return walletCredits;
    }

    public void setWalletCredits(Integer walletCredits) {
        this.walletCredits = walletCredits;
    }

    public Boolean isEligibleForPromoCredit() {
        return eligibleForPromoCredit;
    }

    public void setEligibleForPromoCredit(Boolean eligibleForPromoCredit) {
        this.eligibleForPromoCredit = eligibleForPromoCredit;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<Double> getLocation() {
        return location;
    }

    public void setLocation(List<Double> location) {
        this.location = location;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Provider provider = (Provider) o;
        if(provider.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, provider.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Provider{" +
            "id=" + id +
            ", created='" + created + "'" +
            ", deleted='" + deleted + "'" +
            ", lastUpdate='" + lastUpdate + "'" +
            ", version='" + version + "'" +
            ", name='" + name + "'" +
            ", mobile='" + mobile + "'" +
            ", email='" + email + "'" +
            ", address='" + address + "'" +
            ", imageUrl='" + imageUrl + "'" +
            ", promoCreatedDate='" + promoCreatedDate + "'" +
            ", monthlyFreeCredits='" + monthlyFreeCredits + "'" +
            ", walletCredits='" + walletCredits + "'" +
            ", eligibleForPromoCredit='" + eligibleForPromoCredit + "'" +
            ", active='" + active + "'" +
            ", location='" + location + "'" +
            ", consumerId='" + consumerId + "'" +
            '}';
    }
}
