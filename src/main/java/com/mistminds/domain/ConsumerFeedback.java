package com.mistminds.domain;

import io.swagger.annotations.ApiModelProperty;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A ConsumerFeedback.
 */

@Document(collection = "consumer_feedback")
public class ConsumerFeedback implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("created")
    @CreatedDate
    @JsonIgnore
    private ZonedDateTime created;

    @Field("deleted")
    private ZonedDateTime deleted;

    @Field("last_update")
    @LastModifiedDate
    @JsonIgnore
    private ZonedDateTime lastUpdate;

    @Field("version")
    private Integer version;

    @Field("comment")
    private String comment;

    @Field("like_dislike")
    private String likeDislike;

    @Field("share")
    private String share;

    @Field("favourite")
    private String favourite;

    /**
     * <Enter note text here>                                                  
     * 
     */
    @ApiModelProperty(value = ""
        + "<Enter note text here>                                             "
        + "")
    @Field("count")
    private Integer count;

    @Field("notification_id")
    private String notificationId;

    @Field("consumer_id")
    private String consumerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(ZonedDateTime deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLikeDislike() {
        return likeDislike;
    }

    public void setLikeDislike(String likeDislike) {
        this.likeDislike = likeDislike;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getFavourite() {
        return favourite;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConsumerFeedback consumerFeedback = (ConsumerFeedback) o;
        if(consumerFeedback.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, consumerFeedback.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ConsumerFeedback{" +
            "id=" + id +
            ", created='" + created + "'" +
            ", deleted='" + deleted + "'" +
            ", lastUpdate='" + lastUpdate + "'" +
            ", version='" + version + "'" +
            ", comment='" + comment + "'" +
            ", likeDislike='" + likeDislike + "'" +
            ", share='" + share + "'" +
            ", favourite='" + favourite + "'" +
            ", count='" + count + "'" +
            ", notificationId='" + notificationId + "'" +
            ", consumerId='" + consumerId + "'" +
            '}';
    }
}
