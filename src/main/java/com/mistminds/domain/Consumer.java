package com.mistminds.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

/**
 * A Consumer.
 */

@Document(collection = "consumer")
public class Consumer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("created")
    @CreatedDate
    @JsonIgnore
    private ZonedDateTime created;

    @Field("deleted")
    private ZonedDateTime deleted;

    @Field("last_update")
    @LastModifiedDate
    @JsonIgnore
    private ZonedDateTime lastUpdate;

    @Field("version")
    private Integer version;

    @Field("name")
    private String name;

    @Field("mobile")
    private String mobile;

    @Field("email")
    private String email;

    @Field("status")
    private String status;

    @Field("active")
    private Boolean active;

    @Field("otp")
    private String otp;

    @Field("otp_count")
    private Integer otpCount;

    @Field("device_info")
    private DeviceInfo deviceInfo;

    @Field("location")
    private List<Double> location;

    @Field("unsubscribe_category")
    private List<Category> unsubscribeCategory;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(ZonedDateTime deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Integer getOtpCount() {
        return otpCount;
    }

    public void setOtpCount(Integer otpCount) {
        this.otpCount = otpCount;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public List<Double> getLocation() {
        return location;
    }

    public void setLocation(List<Double> location) {
        this.location = location;
    }

    public List<Category> getUnsubscribeCategory() {
        return unsubscribeCategory;
    }

    public void setUnsubscribeCategory(List<Category> unsubscribeCategory) {
        this.unsubscribeCategory = unsubscribeCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Consumer consumer = (Consumer) o;
        if(consumer.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, consumer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Consumer{" +
            "id=" + id +
            ", created='" + created + "'" +
            ", deleted='" + deleted + "'" +
            ", lastUpdate='" + lastUpdate + "'" +
            ", version='" + version + "'" +
            ", name='" + name + "'" +
            ", mobile='" + mobile + "'" +
            ", email='" + email + "'" +
            ", status='" + status + "'" +
            ", active='" + active + "'" +
            ", otp='" + otp + "'" +
            ", otpCount='" + otpCount + "'" +
            ", deviceInfo='" + deviceInfo + "'" +
            ", location='" + location + "'" +
            ", unsubscribeCategory='" + unsubscribeCategory + "'" +
            '}';
    }
}
