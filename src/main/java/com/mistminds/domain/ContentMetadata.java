package com.mistminds.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A ContentMetadata.
 */

@Document(collection = "content_metadata")
public class ContentMetadata implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("created")
    @CreatedDate
    @JsonIgnore
    private ZonedDateTime created;

    @Field("deleted")
    private ZonedDateTime deleted;

    @Field("last_update")
    @LastModifiedDate
    @JsonIgnore
    private ZonedDateTime lastUpdate;

    @Field("version")
    private Integer version;

    @Field("signature")
    private String signature;

    @Field("format")
    private String format;

    @Field("type")
    private String type;

    @Field("image_version")
    private String imageVersion;

    @Field("url")
    private String url;

    @Field("tags")
    private String tags;

    @Field("bytes")
    private String bytes;

    @Field("width")
    private String width;

    @Field("height")
    private String height;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(ZonedDateTime deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(String imageVersion) {
        this.imageVersion = imageVersion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getBytes() {
        return bytes;
    }

    public void setBytes(String bytes) {
        this.bytes = bytes;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ContentMetadata contentMetadata = (ContentMetadata) o;
        if(contentMetadata.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, contentMetadata.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ContentMetadata{" +
            "id=" + id +
            ", created='" + created + "'" +
            ", deleted='" + deleted + "'" +
            ", lastUpdate='" + lastUpdate + "'" +
            ", version='" + version + "'" +
            ", signature='" + signature + "'" +
            ", format='" + format + "'" +
            ", type='" + type + "'" +
            ", imageVersion='" + imageVersion + "'" +
            ", url='" + url + "'" +
            ", tags='" + tags + "'" +
            ", bytes='" + bytes + "'" +
            ", width='" + width + "'" +
            ", height='" + height + "'" +
            '}';
    }
}
