package com.mistminds.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A NotificationAcknowledgement.
 */

@Document(collection = "notification_acknowledgement")
public class NotificationAcknowledgement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("created")
    @CreatedDate
    @JsonIgnore
    private ZonedDateTime created;

    @Field("deleted")
    private ZonedDateTime deleted;

    @Field("last_update")
    @LastModifiedDate
    @JsonIgnore
    private ZonedDateTime lastUpdate;

    @Field("version")
    private Integer version;

    @Field("read")
    private ZonedDateTime read;

    @Field("delivered")
    private ZonedDateTime delivered;

    @Field("sent")
    private ZonedDateTime sent;

    @Field("visited")
    private ZonedDateTime visited;

    @Field("consumer_id")
    private String consumerId;

    @Field("notification_id")
    private String notificationId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(ZonedDateTime deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public ZonedDateTime getRead() {
        return read;
    }

    public void setRead(ZonedDateTime read) {
        this.read = read;
    }

    public ZonedDateTime getDelivered() {
        return delivered;
    }

    public void setDelivered(ZonedDateTime delivered) {
        this.delivered = delivered;
    }

    public ZonedDateTime getSent() {
        return sent;
    }

    public void setSent(ZonedDateTime sent) {
        this.sent = sent;
    }

    public ZonedDateTime getVisited() {
        return visited;
    }

    public void setVisited(ZonedDateTime visited) {
        this.visited = visited;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NotificationAcknowledgement notificationAcknowledgement = (NotificationAcknowledgement) o;
        if(notificationAcknowledgement.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, notificationAcknowledgement.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "NotificationAcknowledgement{" +
            "id=" + id +
            ", created='" + created + "'" +
            ", deleted='" + deleted + "'" +
            ", lastUpdate='" + lastUpdate + "'" +
            ", version='" + version + "'" +
            ", read='" + read + "'" +
            ", delivered='" + delivered + "'" +
            ", sent='" + sent + "'" +
            ", visited='" + visited + "'" +
            ", consumerId='" + consumerId + "'" +
            ", notificationId='" + notificationId + "'" +
            '}';
    }
}
