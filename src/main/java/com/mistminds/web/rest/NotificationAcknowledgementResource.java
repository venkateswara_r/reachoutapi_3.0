package com.mistminds.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mistminds.domain.NotificationAcknowledgement;
import com.mistminds.repository.NotificationAcknowledgementRepository;
import com.mistminds.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NotificationAcknowledgement.
 */
@RestController
@RequestMapping("/api")
public class NotificationAcknowledgementResource {

    private final Logger log = LoggerFactory.getLogger(NotificationAcknowledgementResource.class);
        
    @Inject
    private NotificationAcknowledgementRepository notificationAcknowledgementRepository;
    
    /**
     * POST  /notification-acknowledgements : Create a new notificationAcknowledgement.
     *
     * @param notificationAcknowledgement the notificationAcknowledgement to create
     * @return the ResponseEntity with status 201 (Created) and with body the new notificationAcknowledgement, or with status 400 (Bad Request) if the notificationAcknowledgement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/notification-acknowledgements",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NotificationAcknowledgement> createNotificationAcknowledgement(@RequestBody NotificationAcknowledgement notificationAcknowledgement) throws URISyntaxException {
        log.debug("REST request to save NotificationAcknowledgement : {}", notificationAcknowledgement);
        if (notificationAcknowledgement.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("notificationAcknowledgement", "idexists", "A new notificationAcknowledgement cannot already have an ID")).body(null);
        }
        NotificationAcknowledgement result = notificationAcknowledgementRepository.save(notificationAcknowledgement);
        return ResponseEntity.created(new URI("/api/notification-acknowledgements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("notificationAcknowledgement", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /notification-acknowledgements : Updates an existing notificationAcknowledgement.
     *
     * @param notificationAcknowledgement the notificationAcknowledgement to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated notificationAcknowledgement,
     * or with status 400 (Bad Request) if the notificationAcknowledgement is not valid,
     * or with status 500 (Internal Server Error) if the notificationAcknowledgement couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/notification-acknowledgements",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NotificationAcknowledgement> updateNotificationAcknowledgement(@RequestBody NotificationAcknowledgement notificationAcknowledgement) throws URISyntaxException {
        log.debug("REST request to update NotificationAcknowledgement : {}", notificationAcknowledgement);
        if (notificationAcknowledgement.getId() == null) {
            return createNotificationAcknowledgement(notificationAcknowledgement);
        }
        NotificationAcknowledgement result = notificationAcknowledgementRepository.save(notificationAcknowledgement);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("notificationAcknowledgement", notificationAcknowledgement.getId().toString()))
            .body(result);
    }

    /**
     * GET  /notification-acknowledgements : get all the notificationAcknowledgements.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of notificationAcknowledgements in body
     */
    @RequestMapping(value = "/notification-acknowledgements",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<NotificationAcknowledgement> getAllNotificationAcknowledgements() {
        log.debug("REST request to get all NotificationAcknowledgements");
        List<NotificationAcknowledgement> notificationAcknowledgements = notificationAcknowledgementRepository.findAll();
        return notificationAcknowledgements;
    }

    /**
     * GET  /notification-acknowledgements/:id : get the "id" notificationAcknowledgement.
     *
     * @param id the id of the notificationAcknowledgement to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the notificationAcknowledgement, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/notification-acknowledgements/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NotificationAcknowledgement> getNotificationAcknowledgement(@PathVariable String id) {
        log.debug("REST request to get NotificationAcknowledgement : {}", id);
        NotificationAcknowledgement notificationAcknowledgement = notificationAcknowledgementRepository.findOne(id);
        return Optional.ofNullable(notificationAcknowledgement)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /notification-acknowledgements/:id : delete the "id" notificationAcknowledgement.
     *
     * @param id the id of the notificationAcknowledgement to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/notification-acknowledgements/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteNotificationAcknowledgement(@PathVariable String id) {
        log.debug("REST request to delete NotificationAcknowledgement : {}", id);
        notificationAcknowledgementRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("notificationAcknowledgement", id.toString())).build();
    }

}
