package com.mistminds.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mistminds.domain.ConsumerFeedback;
import com.mistminds.repository.ConsumerFeedbackRepository;
import com.mistminds.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConsumerFeedback.
 */
@RestController
@RequestMapping("/api")
public class ConsumerFeedbackResource {

    private final Logger log = LoggerFactory.getLogger(ConsumerFeedbackResource.class);
        
    @Inject
    private ConsumerFeedbackRepository consumerFeedbackRepository;
    
    /**
     * POST  /consumer-feedbacks : Create a new consumerFeedback.
     *
     * @param consumerFeedback the consumerFeedback to create
     * @return the ResponseEntity with status 201 (Created) and with body the new consumerFeedback, or with status 400 (Bad Request) if the consumerFeedback has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/consumer-feedbacks",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConsumerFeedback> createConsumerFeedback(@RequestBody ConsumerFeedback consumerFeedback) throws URISyntaxException {
        log.debug("REST request to save ConsumerFeedback : {}", consumerFeedback);
        if (consumerFeedback.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("consumerFeedback", "idexists", "A new consumerFeedback cannot already have an ID")).body(null);
        }
        ConsumerFeedback result = consumerFeedbackRepository.save(consumerFeedback);
        return ResponseEntity.created(new URI("/api/consumer-feedbacks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("consumerFeedback", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /consumer-feedbacks : Updates an existing consumerFeedback.
     *
     * @param consumerFeedback the consumerFeedback to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated consumerFeedback,
     * or with status 400 (Bad Request) if the consumerFeedback is not valid,
     * or with status 500 (Internal Server Error) if the consumerFeedback couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/consumer-feedbacks",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConsumerFeedback> updateConsumerFeedback(@RequestBody ConsumerFeedback consumerFeedback) throws URISyntaxException {
        log.debug("REST request to update ConsumerFeedback : {}", consumerFeedback);
        if (consumerFeedback.getId() == null) {
            return createConsumerFeedback(consumerFeedback);
        }
        ConsumerFeedback result = consumerFeedbackRepository.save(consumerFeedback);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("consumerFeedback", consumerFeedback.getId().toString()))
            .body(result);
    }

    /**
     * GET  /consumer-feedbacks : get all the consumerFeedbacks.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of consumerFeedbacks in body
     */
    @RequestMapping(value = "/consumer-feedbacks",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ConsumerFeedback> getAllConsumerFeedbacks() {
        log.debug("REST request to get all ConsumerFeedbacks");
        List<ConsumerFeedback> consumerFeedbacks = consumerFeedbackRepository.findAll();
        return consumerFeedbacks;
    }

    /**
     * GET  /consumer-feedbacks/:id : get the "id" consumerFeedback.
     *
     * @param id the id of the consumerFeedback to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the consumerFeedback, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/consumer-feedbacks/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConsumerFeedback> getConsumerFeedback(@PathVariable String id) {
        log.debug("REST request to get ConsumerFeedback : {}", id);
        ConsumerFeedback consumerFeedback = consumerFeedbackRepository.findOne(id);
        return Optional.ofNullable(consumerFeedback)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /consumer-feedbacks/:id : delete the "id" consumerFeedback.
     *
     * @param id the id of the consumerFeedback to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/consumer-feedbacks/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteConsumerFeedback(@PathVariable String id) {
        log.debug("REST request to delete ConsumerFeedback : {}", id);
        consumerFeedbackRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("consumerFeedback", id.toString())).build();
    }

}
