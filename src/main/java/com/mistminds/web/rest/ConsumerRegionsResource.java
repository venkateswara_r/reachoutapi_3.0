package com.mistminds.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mistminds.domain.ConsumerRegions;
import com.mistminds.repository.ConsumerRegionsRepository;
import com.mistminds.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConsumerRegions.
 */
@RestController
@RequestMapping("/api")
public class ConsumerRegionsResource {

    private final Logger log = LoggerFactory.getLogger(ConsumerRegionsResource.class);
        
    @Inject
    private ConsumerRegionsRepository consumerRegionsRepository;
    
    /**
     * POST  /consumer-regions : Create a new consumerRegions.
     *
     * @param consumerRegions the consumerRegions to create
     * @return the ResponseEntity with status 201 (Created) and with body the new consumerRegions, or with status 400 (Bad Request) if the consumerRegions has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/consumer-regions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConsumerRegions> createConsumerRegions(@RequestBody ConsumerRegions consumerRegions) throws URISyntaxException {
        log.debug("REST request to save ConsumerRegions : {}", consumerRegions);
        if (consumerRegions.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("consumerRegions", "idexists", "A new consumerRegions cannot already have an ID")).body(null);
        }
        ConsumerRegions result = consumerRegionsRepository.save(consumerRegions);
        return ResponseEntity.created(new URI("/api/consumer-regions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("consumerRegions", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /consumer-regions : Updates an existing consumerRegions.
     *
     * @param consumerRegions the consumerRegions to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated consumerRegions,
     * or with status 400 (Bad Request) if the consumerRegions is not valid,
     * or with status 500 (Internal Server Error) if the consumerRegions couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/consumer-regions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConsumerRegions> updateConsumerRegions(@RequestBody ConsumerRegions consumerRegions) throws URISyntaxException {
        log.debug("REST request to update ConsumerRegions : {}", consumerRegions);
        if (consumerRegions.getId() == null) {
            return createConsumerRegions(consumerRegions);
        }
        ConsumerRegions result = consumerRegionsRepository.save(consumerRegions);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("consumerRegions", consumerRegions.getId().toString()))
            .body(result);
    }

    /**
     * GET  /consumer-regions : get all the consumerRegions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of consumerRegions in body
     */
    @RequestMapping(value = "/consumer-regions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ConsumerRegions> getAllConsumerRegions() {
        log.debug("REST request to get all ConsumerRegions");
        List<ConsumerRegions> consumerRegions = consumerRegionsRepository.findAll();
        return consumerRegions;
    }

    /**
     * GET  /consumer-regions/:id : get the "id" consumerRegions.
     *
     * @param id the id of the consumerRegions to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the consumerRegions, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/consumer-regions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConsumerRegions> getConsumerRegions(@PathVariable String id) {
        log.debug("REST request to get ConsumerRegions : {}", id);
        ConsumerRegions consumerRegions = consumerRegionsRepository.findOne(id);
        return Optional.ofNullable(consumerRegions)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /consumer-regions/:id : delete the "id" consumerRegions.
     *
     * @param id the id of the consumerRegions to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/consumer-regions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteConsumerRegions(@PathVariable String id) {
        log.debug("REST request to delete ConsumerRegions : {}", id);
        consumerRegionsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("consumerRegions", id.toString())).build();
    }

}
