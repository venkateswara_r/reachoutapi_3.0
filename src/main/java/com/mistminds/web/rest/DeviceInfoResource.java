package com.mistminds.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mistminds.domain.DeviceInfo;
import com.mistminds.repository.DeviceInfoRepository;
import com.mistminds.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DeviceInfo.
 */
@RestController
@RequestMapping("/api")
public class DeviceInfoResource {

    private final Logger log = LoggerFactory.getLogger(DeviceInfoResource.class);
        
    @Inject
    private DeviceInfoRepository deviceInfoRepository;
    
    /**
     * POST  /device-infos : Create a new deviceInfo.
     *
     * @param deviceInfo the deviceInfo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deviceInfo, or with status 400 (Bad Request) if the deviceInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/device-infos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DeviceInfo> createDeviceInfo(@RequestBody DeviceInfo deviceInfo) throws URISyntaxException {
        log.debug("REST request to save DeviceInfo : {}", deviceInfo);
        if (deviceInfo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("deviceInfo", "idexists", "A new deviceInfo cannot already have an ID")).body(null);
        }
        DeviceInfo result = deviceInfoRepository.save(deviceInfo);
        return ResponseEntity.created(new URI("/api/device-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("deviceInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /device-infos : Updates an existing deviceInfo.
     *
     * @param deviceInfo the deviceInfo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deviceInfo,
     * or with status 400 (Bad Request) if the deviceInfo is not valid,
     * or with status 500 (Internal Server Error) if the deviceInfo couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/device-infos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DeviceInfo> updateDeviceInfo(@RequestBody DeviceInfo deviceInfo) throws URISyntaxException {
        log.debug("REST request to update DeviceInfo : {}", deviceInfo);
        if (deviceInfo.getId() == null) {
            return createDeviceInfo(deviceInfo);
        }
        DeviceInfo result = deviceInfoRepository.save(deviceInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("deviceInfo", deviceInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /device-infos : get all the deviceInfos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of deviceInfos in body
     */
    @RequestMapping(value = "/device-infos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DeviceInfo> getAllDeviceInfos() {
        log.debug("REST request to get all DeviceInfos");
        List<DeviceInfo> deviceInfos = deviceInfoRepository.findAll();
        return deviceInfos;
    }

    /**
     * GET  /device-infos/:id : get the "id" deviceInfo.
     *
     * @param id the id of the deviceInfo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deviceInfo, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/device-infos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DeviceInfo> getDeviceInfo(@PathVariable String id) {
        log.debug("REST request to get DeviceInfo : {}", id);
        DeviceInfo deviceInfo = deviceInfoRepository.findOne(id);
        return Optional.ofNullable(deviceInfo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /device-infos/:id : delete the "id" deviceInfo.
     *
     * @param id the id of the deviceInfo to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/device-infos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDeviceInfo(@PathVariable String id) {
        log.debug("REST request to delete DeviceInfo : {}", id);
        deviceInfoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("deviceInfo", id.toString())).build();
    }

}
