package com.mistminds.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mistminds.domain.ConsumerFavourite;
import com.mistminds.repository.ConsumerFavouriteRepository;
import com.mistminds.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConsumerFavourite.
 */
@RestController
@RequestMapping("/api")
public class ConsumerFavouriteResource {

    private final Logger log = LoggerFactory.getLogger(ConsumerFavouriteResource.class);
        
    @Inject
    private ConsumerFavouriteRepository consumerFavouriteRepository;
    
    /**
     * POST  /consumer-favourites : Create a new consumerFavourite.
     *
     * @param consumerFavourite the consumerFavourite to create
     * @return the ResponseEntity with status 201 (Created) and with body the new consumerFavourite, or with status 400 (Bad Request) if the consumerFavourite has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/consumer-favourites",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConsumerFavourite> createConsumerFavourite(@RequestBody ConsumerFavourite consumerFavourite) throws URISyntaxException {
        log.debug("REST request to save ConsumerFavourite : {}", consumerFavourite);
        if (consumerFavourite.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("consumerFavourite", "idexists", "A new consumerFavourite cannot already have an ID")).body(null);
        }
        ConsumerFavourite result = consumerFavouriteRepository.save(consumerFavourite);
        return ResponseEntity.created(new URI("/api/consumer-favourites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("consumerFavourite", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /consumer-favourites : Updates an existing consumerFavourite.
     *
     * @param consumerFavourite the consumerFavourite to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated consumerFavourite,
     * or with status 400 (Bad Request) if the consumerFavourite is not valid,
     * or with status 500 (Internal Server Error) if the consumerFavourite couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/consumer-favourites",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConsumerFavourite> updateConsumerFavourite(@RequestBody ConsumerFavourite consumerFavourite) throws URISyntaxException {
        log.debug("REST request to update ConsumerFavourite : {}", consumerFavourite);
        if (consumerFavourite.getId() == null) {
            return createConsumerFavourite(consumerFavourite);
        }
        ConsumerFavourite result = consumerFavouriteRepository.save(consumerFavourite);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("consumerFavourite", consumerFavourite.getId().toString()))
            .body(result);
    }

    /**
     * GET  /consumer-favourites : get all the consumerFavourites.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of consumerFavourites in body
     */
    @RequestMapping(value = "/consumer-favourites",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ConsumerFavourite> getAllConsumerFavourites() {
        log.debug("REST request to get all ConsumerFavourites");
        List<ConsumerFavourite> consumerFavourites = consumerFavouriteRepository.findAll();
        return consumerFavourites;
    }

    /**
     * GET  /consumer-favourites/:id : get the "id" consumerFavourite.
     *
     * @param id the id of the consumerFavourite to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the consumerFavourite, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/consumer-favourites/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConsumerFavourite> getConsumerFavourite(@PathVariable String id) {
        log.debug("REST request to get ConsumerFavourite : {}", id);
        ConsumerFavourite consumerFavourite = consumerFavouriteRepository.findOne(id);
        return Optional.ofNullable(consumerFavourite)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /consumer-favourites/:id : delete the "id" consumerFavourite.
     *
     * @param id the id of the consumerFavourite to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/consumer-favourites/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteConsumerFavourite(@PathVariable String id) {
        log.debug("REST request to delete ConsumerFavourite : {}", id);
        consumerFavouriteRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("consumerFavourite", id.toString())).build();
    }

}
