package com.mistminds.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mistminds.domain.PrivateMessage;
import com.mistminds.repository.PrivateMessageRepository;
import com.mistminds.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PrivateMessage.
 */
@RestController
@RequestMapping("/api")
public class PrivateMessageResource {

    private final Logger log = LoggerFactory.getLogger(PrivateMessageResource.class);
        
    @Inject
    private PrivateMessageRepository privateMessageRepository;
    
    /**
     * POST  /private-messages : Create a new privateMessage.
     *
     * @param privateMessage the privateMessage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new privateMessage, or with status 400 (Bad Request) if the privateMessage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/private-messages",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PrivateMessage> createPrivateMessage(@RequestBody PrivateMessage privateMessage) throws URISyntaxException {
        log.debug("REST request to save PrivateMessage : {}", privateMessage);
        if (privateMessage.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("privateMessage", "idexists", "A new privateMessage cannot already have an ID")).body(null);
        }
        PrivateMessage result = privateMessageRepository.save(privateMessage);
        return ResponseEntity.created(new URI("/api/private-messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("privateMessage", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /private-messages : Updates an existing privateMessage.
     *
     * @param privateMessage the privateMessage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated privateMessage,
     * or with status 400 (Bad Request) if the privateMessage is not valid,
     * or with status 500 (Internal Server Error) if the privateMessage couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/private-messages",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PrivateMessage> updatePrivateMessage(@RequestBody PrivateMessage privateMessage) throws URISyntaxException {
        log.debug("REST request to update PrivateMessage : {}", privateMessage);
        if (privateMessage.getId() == null) {
            return createPrivateMessage(privateMessage);
        }
        PrivateMessage result = privateMessageRepository.save(privateMessage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("privateMessage", privateMessage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /private-messages : get all the privateMessages.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of privateMessages in body
     */
    @RequestMapping(value = "/private-messages",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PrivateMessage> getAllPrivateMessages() {
        log.debug("REST request to get all PrivateMessages");
        List<PrivateMessage> privateMessages = privateMessageRepository.findAll();
        return privateMessages;
    }

    /**
     * GET  /private-messages/:id : get the "id" privateMessage.
     *
     * @param id the id of the privateMessage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the privateMessage, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/private-messages/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PrivateMessage> getPrivateMessage(@PathVariable String id) {
        log.debug("REST request to get PrivateMessage : {}", id);
        PrivateMessage privateMessage = privateMessageRepository.findOne(id);
        return Optional.ofNullable(privateMessage)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /private-messages/:id : delete the "id" privateMessage.
     *
     * @param id the id of the privateMessage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/private-messages/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePrivateMessage(@PathVariable String id) {
        log.debug("REST request to delete PrivateMessage : {}", id);
        privateMessageRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("privateMessage", id.toString())).build();
    }

}
