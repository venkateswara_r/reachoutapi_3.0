package com.mistminds.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mistminds.domain.Consumer;
import com.mistminds.repository.ConsumerRepository;
import com.mistminds.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Consumer.
 */
@RestController
@RequestMapping("/api")
public class ConsumerResource {

    private final Logger log = LoggerFactory.getLogger(ConsumerResource.class);
        
    @Inject
    private ConsumerRepository consumerRepository;
    
    /**
     * POST  /consumers : Create a new consumer.
     *
     * @param consumer the consumer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new consumer, or with status 400 (Bad Request) if the consumer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/consumers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Consumer> createConsumer(@RequestBody Consumer consumer) throws URISyntaxException {
        log.debug("REST request to save Consumer : {}", consumer);
        if (consumer.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("consumer", "idexists", "A new consumer cannot already have an ID")).body(null);
        }
        Consumer result = consumerRepository.save(consumer);
        return ResponseEntity.created(new URI("/api/consumers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("consumer", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /consumers : Updates an existing consumer.
     *
     * @param consumer the consumer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated consumer,
     * or with status 400 (Bad Request) if the consumer is not valid,
     * or with status 500 (Internal Server Error) if the consumer couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/consumers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Consumer> updateConsumer(@RequestBody Consumer consumer) throws URISyntaxException {
        log.debug("REST request to update Consumer : {}", consumer);
        if (consumer.getId() == null) {
            return createConsumer(consumer);
        }
        Consumer result = consumerRepository.save(consumer);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("consumer", consumer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /consumers : get all the consumers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of consumers in body
     */
    @RequestMapping(value = "/consumers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Consumer> getAllConsumers() {
        log.debug("REST request to get all Consumers");
        List<Consumer> consumers = consumerRepository.findAll();
        return consumers;
    }

    /**
     * GET  /consumers/:id : get the "id" consumer.
     *
     * @param id the id of the consumer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the consumer, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/consumers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Consumer> getConsumer(@PathVariable String id) {
        log.debug("REST request to get Consumer : {}", id);
        Consumer consumer = consumerRepository.findOne(id);
        return Optional.ofNullable(consumer)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /consumers/:id : delete the "id" consumer.
     *
     * @param id the id of the consumer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/consumers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteConsumer(@PathVariable String id) {
        log.debug("REST request to delete Consumer : {}", id);
        consumerRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("consumer", id.toString())).build();
    }

}
