package com.mistminds.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mistminds.domain.ApplicationSettings;
import com.mistminds.repository.ApplicationSettingsRepository;
import com.mistminds.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ApplicationSettings.
 */
@RestController
@RequestMapping("/api")
public class ApplicationSettingsResource {

    private final Logger log = LoggerFactory.getLogger(ApplicationSettingsResource.class);
        
    @Inject
    private ApplicationSettingsRepository applicationSettingsRepository;
    
    /**
     * POST  /application-settings : Create a new applicationSettings.
     *
     * @param applicationSettings the applicationSettings to create
     * @return the ResponseEntity with status 201 (Created) and with body the new applicationSettings, or with status 400 (Bad Request) if the applicationSettings has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/application-settings",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ApplicationSettings> createApplicationSettings(@RequestBody ApplicationSettings applicationSettings) throws URISyntaxException {
        log.debug("REST request to save ApplicationSettings : {}", applicationSettings);
        if (applicationSettings.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("applicationSettings", "idexists", "A new applicationSettings cannot already have an ID")).body(null);
        }
        ApplicationSettings result = applicationSettingsRepository.save(applicationSettings);
        return ResponseEntity.created(new URI("/api/application-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("applicationSettings", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /application-settings : Updates an existing applicationSettings.
     *
     * @param applicationSettings the applicationSettings to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated applicationSettings,
     * or with status 400 (Bad Request) if the applicationSettings is not valid,
     * or with status 500 (Internal Server Error) if the applicationSettings couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/application-settings",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ApplicationSettings> updateApplicationSettings(@RequestBody ApplicationSettings applicationSettings) throws URISyntaxException {
        log.debug("REST request to update ApplicationSettings : {}", applicationSettings);
        if (applicationSettings.getId() == null) {
            return createApplicationSettings(applicationSettings);
        }
        ApplicationSettings result = applicationSettingsRepository.save(applicationSettings);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("applicationSettings", applicationSettings.getId().toString()))
            .body(result);
    }

    /**
     * GET  /application-settings : get all the applicationSettings.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of applicationSettings in body
     */
    @RequestMapping(value = "/application-settings",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ApplicationSettings> getAllApplicationSettings() {
        log.debug("REST request to get all ApplicationSettings");
        List<ApplicationSettings> applicationSettings = applicationSettingsRepository.findAll();
        return applicationSettings;
    }

    /**
     * GET  /application-settings/:id : get the "id" applicationSettings.
     *
     * @param id the id of the applicationSettings to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the applicationSettings, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/application-settings/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ApplicationSettings> getApplicationSettings(@PathVariable String id) {
        log.debug("REST request to get ApplicationSettings : {}", id);
        ApplicationSettings applicationSettings = applicationSettingsRepository.findOne(id);
        return Optional.ofNullable(applicationSettings)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /application-settings/:id : delete the "id" applicationSettings.
     *
     * @param id the id of the applicationSettings to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/application-settings/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteApplicationSettings(@PathVariable String id) {
        log.debug("REST request to delete ApplicationSettings : {}", id);
        applicationSettingsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("applicationSettings", id.toString())).build();
    }

}
