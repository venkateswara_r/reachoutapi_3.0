package com.mistminds.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mistminds.domain.ContentMetadata;
import com.mistminds.repository.ContentMetadataRepository;
import com.mistminds.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ContentMetadata.
 */
@RestController
@RequestMapping("/api")
public class ContentMetadataResource {

    private final Logger log = LoggerFactory.getLogger(ContentMetadataResource.class);
        
    @Inject
    private ContentMetadataRepository contentMetadataRepository;
    
    /**
     * POST  /content-metadata : Create a new contentMetadata.
     *
     * @param contentMetadata the contentMetadata to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contentMetadata, or with status 400 (Bad Request) if the contentMetadata has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/content-metadata",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContentMetadata> createContentMetadata(@RequestBody ContentMetadata contentMetadata) throws URISyntaxException {
        log.debug("REST request to save ContentMetadata : {}", contentMetadata);
        if (contentMetadata.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("contentMetadata", "idexists", "A new contentMetadata cannot already have an ID")).body(null);
        }
        ContentMetadata result = contentMetadataRepository.save(contentMetadata);
        return ResponseEntity.created(new URI("/api/content-metadata/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("contentMetadata", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /content-metadata : Updates an existing contentMetadata.
     *
     * @param contentMetadata the contentMetadata to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contentMetadata,
     * or with status 400 (Bad Request) if the contentMetadata is not valid,
     * or with status 500 (Internal Server Error) if the contentMetadata couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/content-metadata",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContentMetadata> updateContentMetadata(@RequestBody ContentMetadata contentMetadata) throws URISyntaxException {
        log.debug("REST request to update ContentMetadata : {}", contentMetadata);
        if (contentMetadata.getId() == null) {
            return createContentMetadata(contentMetadata);
        }
        ContentMetadata result = contentMetadataRepository.save(contentMetadata);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("contentMetadata", contentMetadata.getId().toString()))
            .body(result);
    }

    /**
     * GET  /content-metadata : get all the contentMetadata.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of contentMetadata in body
     */
    @RequestMapping(value = "/content-metadata",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ContentMetadata> getAllContentMetadata() {
        log.debug("REST request to get all ContentMetadata");
        List<ContentMetadata> contentMetadata = contentMetadataRepository.findAll();
        return contentMetadata;
    }

    /**
     * GET  /content-metadata/:id : get the "id" contentMetadata.
     *
     * @param id the id of the contentMetadata to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contentMetadata, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/content-metadata/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContentMetadata> getContentMetadata(@PathVariable String id) {
        log.debug("REST request to get ContentMetadata : {}", id);
        ContentMetadata contentMetadata = contentMetadataRepository.findOne(id);
        return Optional.ofNullable(contentMetadata)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /content-metadata/:id : delete the "id" contentMetadata.
     *
     * @param id the id of the contentMetadata to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/content-metadata/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteContentMetadata(@PathVariable String id) {
        log.debug("REST request to delete ContentMetadata : {}", id);
        contentMetadataRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("contentMetadata", id.toString())).build();
    }

}
